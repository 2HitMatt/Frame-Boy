#ifndef UIRADIOGROUP
#define UIRADIOGROUP

#include "uiElement.h"
#include "uiCheckbox.h"

//this thing treats a bunch of checkboxes as a radio button group. Somehow
class UIRadioGroup : public UIElement{
	public:
		list<UICheckbox*> radioButtons;

		UICheckbox *lastChecked = NULL;

		void update();
};

#endif