#include "uiHelpBox.h"

UIHelpBox::~UIHelpBox(){
	info.removeAll(true);
}
void UIHelpBox::onClick(UIMouseEvent &mouseEvent){
	if (Globals::showHelp && active && UIElement::checkMouseClick(*this, mouseEvent)){
		info.onClick(mouseEvent);

		if (!mouseEvent.mouseEventUsed){
			active = false;
			mouseEvent.mouseEventUsed = true;
		}

	}
}
void UIHelpBox::onDrag(UIMouseEvent &mouseEvent){
	if (Globals::showHelp && active && UIElement::checkMouseClick(*this, mouseEvent), UIMouseEvent::MOUSE_LEFTBUTTON, UIMouseEvent::MOUSE_DRAG){
		info.onDrag(mouseEvent);
	}
}
void UIHelpBox::onRelease(UIMouseEvent &mouseEvent){
	if (Globals::showHelp && active && UIElement::checkMouseClick(*this, mouseEvent), UIMouseEvent::MOUSE_LEFTBUTTON, UIMouseEvent::MOUSE_RELEASE){
		info.onRelease(mouseEvent);
	}
}
void UIHelpBox::setup(int x, int y, int w, int h, int depth){
	this->depth = depth;
	uiRegion.x = x;
	uiRegion.y = y;
	uiRegion.w = w;
	uiRegion.h = h;

	

	info.setup(x + padding, y + padding, w - 2 * padding, h - 2 * padding, true);
}
void UIHelpBox::addRowOfText(string str){
	UIText* text = new UIText;
	text->text = str;
	text->font = Globals::secondaryFont;
	text->textColour.r = text->textColour.g = text->textColour.b = 255;
	text->generateText();
	info.uiList.push_back(text);
}

//doesn't work :/
void UIHelpBox::updateHeight(){
	info.updateUIPositions();
	uiRegion.h = info.accumulativeHeight + 2 * padding;
	info.uiRegion.h = info.accumulativeHeight;
	info.updateUIPositions();
}

void UIHelpBox::update(){
	info.update();
}

void UIHelpBox::draw(){
	if (Globals::showHelp && active){
		SDL_SetRenderDrawBlendMode(Globals::renderer, SDL_BLENDMODE_BLEND);
		SDL_SetRenderDrawColor(Globals::renderer, 0, 0, 0, SDL_ALPHA_OPAQUE*0.9);
		SDL_RenderFillRect(Globals::renderer, &uiRegion);
		SDL_SetRenderDrawBlendMode(Globals::renderer, SDL_BLENDMODE_NONE);
		info.draw();
	}
}