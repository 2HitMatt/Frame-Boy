#include "createAnimationSetScreen.h"

CreateAnimationSetScreen::CreateAnimationSetScreen(){
	UIElement::selectedUIElement = NULL;

	nameInput.setup("Animation Set Name:", Globals::primaryFont);
	nameInput.inputMax = 20;
	nameInput.uiRegion.x = 10;
	nameInput.uiRegion.y = 10;
	nameInput.generateButton();
	uiElements.push_back(&nameInput);

	imageText.text = "Image File: ";
	imageText.uiRegion.x = 10;
	imageText.uiRegion.y = 50;
	uiElements.push_back(&imageText);


	string resPath = getResourcePath();
	Globals::filesInADir(images, resPath + "frames", ".png");
	images.size();

	for (list<string>::iterator image = images.begin(); image != images.end(); image++)
	{
		SelectableButton *anim = new SelectableButton();
		anim->setup(*image, Globals::secondaryFont);
		anim->generateButton();
		imageFilesList.uiList.push_back(anim);
	}

	saveButton.setup("Save", Globals::primaryFont);
	saveButton.uiRegion.x = 10;
	saveButton.generateButton();
	saveButton.uiRegion.y = Globals::screenHeight - saveButton.uiRegion.h - 10;
	uiElements.push_back(&saveButton);

	cancelButton.setup("Cancel", Globals::primaryFont);
	cancelButton.uiRegion.x = 300;
	cancelButton.generateButton();
	cancelButton.uiRegion.y = Globals::screenHeight - cancelButton.uiRegion.h - 10;
	uiElements.push_back(&cancelButton);

	imageFilesList.setup(20, 150, Globals::screenWidth*0.6, saveButton.uiRegion.y - 20 - 200);
	uiElements.push_back(&imageFilesList);

	help.setup(300, 50, 400, 50);
	help.addRowOfText("Animation Sets work with 1 spritesheet");
	uiElements.push_back(&help);
}
void CreateAnimationSetScreen::uiUpdates(){
	//selectedImageFile = "";
	for (list<UIElement*>::iterator btn = imageFilesList.uiList.begin(); btn != imageFilesList.uiList.end(); btn++){
		SelectableButton* sBtn = (SelectableButton*)(*btn);

		if (sBtn->selected){
			selectedImageFile = sBtn->text;
			imageText.changeText("Image File: "+selectedImageFile);
		}
	}
	
	if (saveButton.clicked && selectedImageFile != ""){
		//create animationSet file
		AnimationSet newAnimSet(nameInput.inputText + Globals::currentRuleset.fileExt, selectedImageFile);
		newAnimSet.saveAnimationSet();

		newAnimSetFile = nameInput.inputText + Globals::currentRuleset.fileExt;
		UIElement::selectedUIElement = NULL;
		quit = true;
	}
	else{
		saveButton.clicked = false;
	}
	if (cancelButton.clicked){
		UIElement::selectedUIElement = NULL;
		quit = true;
	}
}