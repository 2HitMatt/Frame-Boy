#include "uiButton.h"

void UIButton::useDefaultColours(){
	//dark gray
	buttonColour.r = 47;
	buttonColour.g = 46;
	buttonColour.b = 38;

	//orange
	buttonBorderColour.r = 255;
	buttonBorderColour.g = 63;
	buttonBorderColour.b = 0;

	//orange
	buttonSelectedColour.r = 255;
	buttonSelectedColour.g = 63;
	buttonSelectedColour.b = 0;

	//white
	buttonTextColour.r = 255;
	buttonTextColour.g = 255;
	buttonTextColour.b = 255;
}

void UIButton::setup(string text, TTF_Font* font){
	//setup
	this->text = text;
	this->font = font;
	useDefaultColours();
}

void UIButton::generateButton(){
	if (buttonTextTexture != NULL)
		cleanup(buttonTextTexture);
	
	buttonTextTexture = renderText(text, font, buttonTextColour, Globals::renderer);

	//work out new button size
	int W, H;
	SDL_QueryTexture(buttonTextTexture, NULL, NULL, &W, &H);
	
	if (!fixedWidth)
		uiRegion.w = W + 4;
	
	uiRegion.h = H + 4;
}

//override uielements draw
void UIButton::draw(){
	if (buttonTextTexture == NULL)
		generateButton();

	if (selected)
		SDL_SetRenderDrawColor(Globals::renderer, buttonSelectedColour.r, buttonSelectedColour.g, buttonSelectedColour.b, SDL_ALPHA_OPAQUE);
	else
		SDL_SetRenderDrawColor(Globals::renderer, buttonColour.r, buttonColour.g, buttonColour.b,SDL_ALPHA_OPAQUE);
	SDL_RenderFillRect(Globals::renderer, &uiRegion);

	SDL_SetRenderDrawColor(Globals::renderer, buttonBorderColour.r, buttonBorderColour.g, buttonBorderColour.b, SDL_ALPHA_OPAQUE);
	SDL_RenderDrawRect(Globals::renderer, &uiRegion);

	renderTexture(buttonTextTexture, Globals::renderer, uiRegion.x + 2, uiRegion.y + 2);
}