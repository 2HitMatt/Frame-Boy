#include "uiScreen.h"

void UIScreen::update(){ 
	
	
	//start text input
	SDL_StartTextInput();

	

	SDL_Event e;
	while (!quit)
	{
		
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			//Special key input
			else if (e.type == SDL_KEYDOWN)
			{
				//Handle backspace
				if (e.key.keysym.sym == SDLK_BACKSPACE && UIElement::selectedUIElement != NULL)
				{
					//lop off character
					UIElement::selectedUIElement->onBackSpace();
				}
				//Handle copy
				else if (e.key.keysym.sym == SDLK_c && SDL_GetModState() & KMOD_CTRL && UIElement::selectedUIElement != NULL)
				{
					UIElement::selectedUIElement->copy();
				}
				//Handle paste
				else if (e.key.keysym.sym == SDLK_v && SDL_GetModState() & KMOD_CTRL && UIElement::selectedUIElement != NULL)
				{
					UIElement::selectedUIElement->paste();
				}
				else{
					//pass keypress to those wanting to listen in on it
					for (list<UIElement*>::iterator ui = keyPressElements.begin(); ui != keyPressElements.end(); ui++){
						(*ui)->onKeyPressed(e.key.keysym.sym);
					}
				}

			}
			//Special text input event
			else if (e.type == SDL_TEXTINPUT)
			{
				//Not copy or pasting
				if (UIElement::selectedUIElement != NULL && !((e.text.text[0] == 'c' || e.text.text[0] == 'C') && (e.text.text[0] == 'v' || e.text.text[0] == 'V') && SDL_GetModState() & KMOD_CTRL))
				{
					//Append character
					UIElement::selectedUIElement->onTyped(e.text.text[0]);
				}
			}
			else if (e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP || (e.type == SDL_MOUSEMOTION && e.motion.state == SDL_PRESSED)){
				UIMouseEvent mouseEvent;
				mouseEvent.updateEvent(&e);

				//sort elements from top to bottom for interaction
				uiElements.sort(UIElement::UIElementCompareDepthForInteraction);

				for (list<UIElement*>::iterator ui = uiElements.begin(); ui != uiElements.end(); ui++)
				{
					if (mouseEvent.type == UIMouseEvent::MOUSE_CLICK)
						(*ui)->onClick(mouseEvent);
					if (mouseEvent.type == UIMouseEvent::MOUSE_RELEASE)
						(*ui)->onRelease(mouseEvent);
					if (mouseEvent.type == UIMouseEvent::MOUSE_DRAG)
						(*ui)->onDrag(mouseEvent);

					//someone registerd the click
					if (mouseEvent.mouseEventUsed)
						break;
				}
			}

			
		}

		//update and draw ui elements at the same time (save a loop run :/)
		for (list<UIElement*>::iterator ui = uiElements.begin(); ui != uiElements.end(); ui++)
		{
			(*ui)->update();
		}

		//put your custom code in this ui update function ;)
		uiUpdates();

		draw();
	
	}
	
	//Disable text input
	SDL_StopTextInput();
}
void UIScreen::draw(){ 
	//Clear screen
	SDL_SetRenderDrawColor(Globals::renderer, 0xFF, 0xFF, 0xFF, 0xFF);
	SDL_RenderClear(Globals::renderer);

	uiElements.sort(UIElement::UIElementCompareDepthForDraw);
	for (list<UIElement*>::iterator ui = uiElements.begin(); ui != uiElements.end(); ui++)
	{
		(*ui)->draw();
	}


	//Update screen
	SDL_RenderPresent(Globals::renderer);
}