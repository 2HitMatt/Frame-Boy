#include "uiImage.h"

void UIImage::setup(SDL_Texture* texture, SDL_Rect clip){
	this->texture = texture;
	this->clip = clip;

	uiRegion.w = clip.w;
	uiRegion.h = clip.h;
}
void UIImage::draw(){
	renderTexture(texture, Globals::renderer, uiRegion.x, uiRegion.y, &clip);
}