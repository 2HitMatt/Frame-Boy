#ifndef UICHECKBOX
#define UICHECKBOX

#include "basicButton.h"

class UICheckbox : public UIButton{
public:
	SDL_Texture* checkboxCheckedTexture = NULL; //X for checked, or some other chara, whatever
	bool checked = false;

	void generateButton();
	void onClick(UIMouseEvent &mouseEvent);
	void draw();

};
#endif