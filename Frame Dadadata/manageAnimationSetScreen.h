#ifndef MANAGEANIMATIONSETSCREEN
#define MANAGEANIMATIONSETSCREEN

#include "uiScreen.h"
#include "uiText.h"
#include "uiList.h"
#include "uiRow.h"
#include "basicButton.h"
#include "AnimationSet.h"
#include "createAnimationScreen.h"
#include "manageAnimationScreen.h"


class ManageAnimationSetScreen : public UIScreen{
public:
	UIList anims;
	
	UIText animsetName;

	BasicButton addButton;
	bool exported = false;
	BasicButton jsonButton;
	BasicButton backButton;

	AnimationSet animSet;

	ManageAnimationSetScreen(string animationSetFile);
	~ManageAnimationSetScreen();
	void uiUpdates();

	void addRow(string animName);
};


#endif