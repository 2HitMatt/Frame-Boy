#include "createAnimationScreen.h"

CreateAnimationScreen::CreateAnimationScreen(){
	UIElement::selectedUIElement = NULL;

	nameInput.setup("Animation Name: ", Globals::primaryFont);
	nameInput.inputMax = 32;
	nameInput.uiRegion.x = 10;
	nameInput.uiRegion.y = 10;
	nameInput.generateButton();
	uiElements.push_back(&nameInput);

	saveButton.setup("Save", Globals::primaryFont);
	saveButton.uiRegion.x = 10;
	saveButton.uiRegion.y = 300;
	saveButton.generateButton();
	uiElements.push_back(&saveButton);

	cancelButton.setup("Cancel", Globals::primaryFont);
	cancelButton.uiRegion.x = 300;
	cancelButton.uiRegion.y = 300;
	cancelButton.generateButton();
	uiElements.push_back(&cancelButton);


}
void CreateAnimationScreen::uiUpdates(){
	if (saveButton.clicked && nameInput.inputText !=  ""){
		animName = nameInput.inputText;
		UIElement::selectedUIElement = NULL;
		quit = true;
	}
	else{
		saveButton.clicked = false;
	}
	if (cancelButton.clicked){
		UIElement::selectedUIElement = NULL;
		quit = true;
	}
}