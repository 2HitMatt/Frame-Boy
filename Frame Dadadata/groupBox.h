#ifndef GROUPBOX_H
#define GROUPBOX_H

#include "group.h"

class GroupBox : public Group{
public:
	list<SDL_Rect> data;

	GroupBox(DataGroupType type){
		this->type = type;
	}

	int numberOfDataInGroup(){
		return data.size();
	}
	void outputGroup(ostream &stream, bool inGroups){
		for (list < SDL_Rect>::iterator box = data.begin(); box != data.end(); box++){

			if (!inGroups)
				stream << type.groupName << ": ";

			stream << (*box).x << " " << (*box).y << " " << (*box).w << " " << (*box).h << endl;
		}
	}
	void exportToJSON(ostream &stream, bool inGroups){
		stream <<"\""<< type.groupName << "\":[";
		for (list < SDL_Rect>::iterator box = data.begin(); box != data.end(); box++){
			stream <<"{"<< Globals::jsonBoxOutput(*box)<<"}";
			
			list < SDL_Rect>::iterator tmp = box;
			tmp++;
			if (tmp != data.end())
				stream << "," << endl;

		}
		stream << "]";
	}
	void addToGroup(string str){
		//hope this works :/
		stringstream ss;
		ss << str;
		SDL_Rect box;
		ss >> box.x >> box.y >> box.w >> box.h;

		data.push_back(box);
	}

	void draw(){
		//TODO
	}

};


#endif