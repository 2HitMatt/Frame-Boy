#include "configFile.h"

void ConfigFile::loadConfig(){
	string resPath = getResourcePath();
	ifstream myFile(resPath + "configFile.txt");

	if (myFile.is_open())
	{
		if (!myFile.eof())
		{
			string buffer;
			getline(myFile, buffer);
			stringstream ss; 
			ss >> fullscreen;

			getline(myFile,lastRuleSet);
		}
	}
	myFile.close();
}

void ConfigFile::saveConfig(){
	string resPath = getResourcePath();
	ofstream myFile(resPath + "configFile.txt");
	if (myFile.is_open())
	{
		myFile << fullscreen << endl;
		myFile << lastRuleSet << endl;
	}
	myFile.close();

}