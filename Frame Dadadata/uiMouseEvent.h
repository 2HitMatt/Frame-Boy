#ifndef UIMOUSEEVENT
#define UIMOUSEEVENT

#include "SDL.h"

class UIMouseEvent{
public:
	static const int MOUSE_CLICK;
	static const int MOUSE_RELEASE;
	static const int MOUSE_DRAG;

	static const int MOUSE_LEFTBUTTON;
	static const int MOUSE_RIGHTBUTTON;
	static const int MOUSE_MIDDLEBUTTON;

	int x, y; //where the mouse action is happening
	int xRel, yRel; //for dragging. Is it useful? We'll find out
	int button; //left, right or middle
	int type; //click, release, drag
	bool mouseEventUsed; //no, havent found a uielement i align with, yes, i've already been spent clicking on something else

	void updateEvent(SDL_Event* event);

};

#endif