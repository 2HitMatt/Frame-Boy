#include "startScreen.h"

StartScreen::StartScreen(){
	UIElement::selectedUIElement = NULL;
	
	currentRuleSet.text = "Current Ruleset: " + Globals::currentRuleset.name;
	currentRuleSet.font = Globals::secondaryFont;
	currentRuleSet.uiRegion.x = 10;
	currentRuleSet.uiRegion.y = 0;
	uiElements.push_back(&currentRuleSet);

	createRuleSet.setup("Create Ruleset",Globals::primaryFont);
	createRuleSet.uiRegion.x = 10;
	createRuleSet.uiRegion.y = 40;
	uiElements.push_back(&createRuleSet);

	manageRuleSets.setup("Manage Ruleset", Globals::primaryFont);
	manageRuleSets.uiRegion.x = 10;
	manageRuleSets.uiRegion.y =80;
	uiElements.push_back(&manageRuleSets);

	createAnimationSet.setup("Create Animation Set", Globals::primaryFont);
	createAnimationSet.uiRegion.x = 10;
	createAnimationSet.uiRegion.y = 120;
	uiElements.push_back(&createAnimationSet);


	loadAnimationSet.setup("Load Animation Set", Globals::primaryFont);
	loadAnimationSet.uiRegion.x = 10;
	loadAnimationSet.uiRegion.y = 160;
	uiElements.push_back(&loadAnimationSet);
	/*
	BasicButton helpButton;
	BasicButton websiteButton;
	BasicButton patreonButton;*/
	helpButton.setup("Show/Hide Help", Globals::primaryFont);
	helpButton.uiRegion.x = 10;
	helpButton.uiRegion.y = 240;
	uiElements.push_back(&helpButton);

	websiteButton.setup("Visit 2hitstudio.com", Globals::primaryFont);
	websiteButton.uiRegion.x = 10;
	websiteButton.uiRegion.y = 280;
	uiElements.push_back(&websiteButton);

	patreonButton.setup("Support 2HitMatt", Globals::primaryFont);
	patreonButton.uiRegion.x = 10;
	patreonButton.uiRegion.y = 320;
	uiElements.push_back(&patreonButton);

	exitButton.uiRegion.x = 10;
	exitButton.uiRegion.y = 400;
	uiElements.push_back(&exitButton);

	help.setup(Globals::screenWidth / 2-8 , 20, Globals::screenWidth / 2 , 400);
	help.addRowOfText("HelpBox:");
	help.addRowOfText("Rulesets: describe what data a frame can have.");
	help.addRowOfText("One ruleset active at a time.");
	help.addRowOfText("Default ruleset: pivot and duration only");
	help.addRowOfText(" ");
	help.addRowOfText("Animation Sets: holds a group of ");
	help.addRowOfText("animations.");
	help.addRowOfText(" ");
	help.addRowOfText("Help Box: To close this box, click on border");
	help.addRowOfText(" ");
	help.addRowOfText(" ");
	help.addRowOfText("Created by 2 Hit Matt from 2 Hit Studio");
	help.addRowOfText("Support Matt via Patreon for more dev :D");

	uiElements.push_back(&help);

	string resPath = getResourcePath();
	logoTexture = loadTexture(resPath + "logo-small.png", Globals::renderer);

	SDL_Rect clip = { 0, 0, 0, 0 };
	SDL_QueryTexture(logoTexture, NULL, NULL, &clip.w, &clip.h);

	logo.setup(logoTexture, clip);
	logo.uiRegion.x = Globals::screenWidth - clip.w - 10;
	logo.uiRegion.y = 20;
	uiElements.push_back(&logo);
}
void StartScreen::uiUpdates(){
	if (currentRuleSet.text != "Current Ruleset: " + Globals::currentRuleset.name)
	{
		currentRuleSet.changeText("Current Ruleset: " + Globals::currentRuleset.name);
	}

	if (createRuleSet.clicked){
		//make sure we do this... idiot!
		createRuleSet.clicked = false;

		CreateRulesetScreen createRulesetScreen;
		createRulesetScreen.update();
	}
	else if (manageRuleSets.clicked){
		manageRuleSets.clicked = false;

		ManageRulesetScreen manageRulesetScreen;
		manageRulesetScreen.update();

		if (!manageRulesetScreen.quit)
		{
			currentRuleSet.changeText("Ruleset: " + Globals::currentRuleset.name);
		}
	}
	else if (createAnimationSet.clicked){
		createAnimationSet.clicked = false;

		CreateAnimationSetScreen createAnimationSetScreen;
		createAnimationSetScreen.update();
		
		//created animationset
		if (createAnimationSetScreen.saveButton.clicked && createAnimationSetScreen.newAnimSetFile != ""){
			//load this animation and do stuff on it on the next screen
			ManageAnimationSetScreen manageAnimSetScreen(createAnimationSetScreen.newAnimSetFile);
			manageAnimSetScreen.update();
		}
	}
	else if (loadAnimationSet.clicked){
		loadAnimationSet.clicked = false;

		LoadAnimationSetScreen loadAnimationSetScreen;
		loadAnimationSetScreen.update();


		if (loadAnimationSetScreen.animToLoad != ""){
			//load this animation and do stuff on it on the next screen
			ManageAnimationSetScreen manageAnimSetScreen(loadAnimationSetScreen.animToLoad);
			manageAnimSetScreen.update();
		}
	}
	else if (helpButton.clicked){
		//toggle help on and off
		Globals::showHelp = !Globals::showHelp;
		helpButton.clicked = false;
	}
	else if (websiteButton.clicked){
		//open 2hit website
		ShellExecute(NULL, "open", "http://2hitstudio.com",
			NULL, NULL, SW_SHOWNORMAL);
		websiteButton.clicked = false;
	}
	else if (patreonButton.clicked){
		//open my patreon
		ShellExecute(NULL, "open", "https://www.patreon.com/wanderinghobo",
			NULL, NULL, SW_SHOWNORMAL);
		patreonButton.clicked = false;
	}

	if (exitButton.quit)
		quit = true;

	//clear this every loop
	UIElement::selectedUIElement = NULL;
}