#ifndef UIROW
#define UIROW

#include "uiElement.h"

//is a horizontal list of elements, e.g nav bar
class UIRow : public UIElement{
public:
	list<UIElement*> uiElements;
	SDL_Color rowColour; //default is clear

	UIRow();

	void onClick(UIMouseEvent &mouseEvent);
	void onDrag(UIMouseEvent &mouseEvent);
	void onRelease(UIMouseEvent &mouseEvent);

	void updatePositions();
	void draw();

	void removeInActive(bool deleteThem = false);
	void removeAll(bool deleteThem = false);
};

#endif