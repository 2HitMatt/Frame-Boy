#ifndef UITEXT
#define UITEXT

#include "drawing_functions.h"
#include "uiElement.h"

class UIText : public UIElement{
public:
	string text;
	SDL_Color textColour;
	SDL_Texture* textTexture = NULL;
	TTF_Font *font;

	UIText(){
		font = Globals::smallFont;
		textColour.r = 0;
		textColour.b = 0;
		textColour.g = 0;
		textColour.a = SDL_ALPHA_OPAQUE;
	}
	~UIText(){
		if (textTexture != NULL)
			cleanup(textTexture);
	}
	void generateText();
	void changeText(string text);
	//override uielements draw
	void draw();

};

#endif