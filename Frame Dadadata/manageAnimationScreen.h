#ifndef MANAGEANIMATIONSCREEN
#define MANAGEANIMATIONSCREEN

#include "uiScreen.h"
#include "uiList.h"
#include "uiRow.h"
#include "uiText.h"
#include "uiImage.h"
#include "basicButton.h"
#include "AnimationSet.h"
#include "addFrameScreen.h"
#include "editFrameScreen.h"
#include "uiAnimation.h"

class ManageAnimationScreen : public UIScreen{
public:
	UIText animName;

	BasicButton addFrameBtn;

	UIList frameList;

	UIAnimation animationUI;

	BasicButton backBtn;
	
	AnimationSet *animSet;
	Animation *currentAnim;
	
	ManageAnimationScreen(AnimationSet *animSet, Animation *currentAnim);
	void uiUpdates();
	void addRow(Frame *frame);
	void redoFrameNumbers();
	void reloadList();

};

#endif