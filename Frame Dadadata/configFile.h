#ifndef CONFIGFILE_H
#define CONFIGFILE_H

#include <string>
#include <sstream>
#include <fstream>
#include "res_path.h"

using namespace std;

class ConfigFile
{
public:
	//set to default values first
	int fullscreen = 1; //1 true, 0 false
	string lastRuleSet = "";

	void loadConfig();
	void saveConfig();
};

#endif