#ifndef RULESET_H
#define RULESET_H

#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#include <list>
#include "res_path.h"
#include "dataGroupType.h"

using namespace std;
/**
Rulesets describe the default things a frame should have, as well as how to save and load a set of animations
*/
class Ruleset{
	public:
		//Basic Attributes
		string name; //name of ruleset
		string fileExt = ".fdset"; //what types of atlas or framedata files does this ruleset apply

		bool saveInGroups; //if yes, output of groups is: group name, followed by a number of lines of items belonging to that group. If no, groupName: x y etc per line in output
		bool outputNumberOfElementsInGroup; //if saveInGroups is true and this is true, then the output is Group : 6, then the 6 elements line by line
		
		list<DataGroupType> groups; //the groups this ruleset takes care of

		//Functions
		void saveRuleset(); //save just the ruleset
		void loadRuleset(string ruleSetName); //load a ruleset up

		//statics
		static const string ruleSetPath;
};


#endif