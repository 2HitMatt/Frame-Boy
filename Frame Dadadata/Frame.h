#ifndef Frame_header
#define Frame_header

#include <iostream>
#include <list>
#include <fstream>
#include "SDL.h"
#include "globals.h"
#include "groupBuilder.h"
#include "ruleset.h"

using namespace std;

class Frame
{
public:
		//Attributes
		SDL_Texture *spriteSheetPtr; //pointer to a full sprite sheet
		int frameNumber; //referred to as 'index' in the output files
		SDL_Rect clip; //its the region on the spritesheet where this frame is. SDL_Rect is a rectangle, 			//x,y,w,h
		float duration; //how long does this frame run for
		SDL_Point offSet; //pivot, x/y offset, whatever you wanna call it
		
		list<Group*> frameData; //the whole fucking reason I'm making this piece of crap

		//Functions
		Frame(SDL_Texture *spriteSheetPtr); //generic for save/load
		Frame(SDL_Texture *spriteSheetPtr, int frameNumber, SDL_Rect clip); //used for creating a new frame that auto fills out groups based on current ruleset
		void Draw(bool direction, float x, float y);
		void DrawRectangles(list<SDL_Rect> &rects, float x, float y, bool direction);
		//save and load from a file, using the current ruleset
		void loadFrame(ifstream &file);
		void saveFrame(ofstream &file);
		void exportToJSON(ofstream &file);
		//NOTE: if this project is used to accomodate other packages, this may change based on ruleset(updated ruleset)

		static bool sortFrameByFrameNumber(const Frame&   a, const Frame&   b)
		{
			//reverse if not stacked properly
			return a.frameNumber < b.frameNumber;

		}

};


#endif