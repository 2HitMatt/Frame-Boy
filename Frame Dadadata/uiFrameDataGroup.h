/*
TODO
-needs a dataGroupType passed to it
-needs a reference tp the current frame
-needs label, add button, and list of rows
-each row has either a button or a textinput, depending on datagrouptype (string, box, point etc)

-loop through datagroups in the frame
-for each data item, build appropriate action ui (button or input), store in a row
-height of this ui element is label plus all the rows' heights

-clicking on a data item will setup the editor properly, and wait for updates, somehow
-clicking add will build 1 new row with default empty values

Note! main hurdles include somehow linking this to the editFrameClass :S (needs an editor reference?)


*/
#ifndef UIFRAMEDATAGROUP
#define UIFRAMEDATAGROUP

#include "uiRow.h"
#include "AnimationSet.h"
#include "basicButton.h"
#include "toggleButton.h"
#include "uiTextInput.h"
#include "groupBuilder.h"
#include "uiImageEditor.h"

class UIFrameDataGroup : public UIElement
{
public:
	Group* group; //the group of data this section is responsible for
	UIImageEditor *editor; //reference back to the ui image editor (so we change modes ;))

	BasicButton label;//label for this group. Is a button, because in the future, clicking this will hide/show data elements in the group
	BasicButton addButton;//add data to a group (based on group)

	UIElement* lastSelected = NULL;

	list<UIRow*> data;

	UIFrameDataGroup(Group *group, UIImageEditor *editor); 
	void deleteData(int row);
	void updateDataRow(int row);
	void addRow(string text);
	void addEmptyRow();
	void updatePositions(); //after adding or deleting rows, work this out. Also, work out new uiRegion.h for this thing
	void onClick(UIMouseEvent &mouseEvent);
	void update();
	void draw();
};

#endif