#include "ruleset.h"

void Ruleset::saveRuleset(){
	string resPath = getResourcePath();
	ofstream file;
	file.open(resPath+ruleSetPath + name + ".txt");

	if (file.good()){

		file << name << endl;
		file << fileExt << endl;
		file << saveInGroups << endl;
		file << outputNumberOfElementsInGroup << endl;
		for (list<DataGroupType>::iterator dgt = groups.begin(); dgt != groups.end(); dgt++)
		{
			DataGroupType::saveRSDataGroupType(file, *dgt);
		}
	}
	else
	{
		//CRASH, BURN, TELL SOMEONE ABOUT IT
	}
	file.close();

}
void Ruleset::loadRuleset(string ruleSetName){
	string resPath = getResourcePath();
	ifstream file;
	file.open(resPath+ruleSetPath + ruleSetName + ".txt");

	if (file.good())
	{

		getline(file, name);
		getline(file, fileExt);
		string buffer;
		getline(file, buffer);
		stringstream ss;
		ss << buffer;
		ss >> saveInGroups;
		ss.clear();
		getline(file, buffer);
		ss << buffer;
		ss >> outputNumberOfElementsInGroup;
		
		groups.clear();
		while (file.good()){
			DataGroupType dgt = DataGroupType::loadRSDataGroupType(file);
			if (dgt.groupName != "" && !dgt.groupName.empty())
				groups.push_back(dgt);
		}
	}
	else{
		//AGHHHHHHHHHHHHHH!!!!!
	}
	file.close();
}

const string Ruleset::ruleSetPath = "rulesets/";