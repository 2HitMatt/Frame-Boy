#ifndef CREATEANIMATIONSCREEN
#define CREATEANIMATIONSCREEN

#include "uiScreen.h"
#include "uiTextInput.h"
#include "basicButton.h"

class CreateAnimationScreen : public UIScreen{
public:
	UITextInput nameInput;

	BasicButton saveButton, cancelButton;

	string animName;

	CreateAnimationScreen();
	void uiUpdates();


};

#endif