#ifndef UISCREEN
#define UISCREEN

#include "uiElement.h"

class UIScreen : public UIElement{
public:
	bool quit = false;
	list<UIElement*> uiElements;
	list<UIElement*> keyPressElements;

	virtual void update();
	virtual void uiUpdates(){/*override me. For specific updates*/ }
	virtual void draw();
};

#endif