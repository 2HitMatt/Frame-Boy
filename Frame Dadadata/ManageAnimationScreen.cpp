#include "manageAnimationScreen.h"

ManageAnimationScreen::ManageAnimationScreen(AnimationSet *animSet, Animation *currentAnim){
	UIElement::selectedUIElement = NULL;
	
	this->animSet = animSet;
	this->currentAnim = currentAnim;

	animationUI.setup(currentAnim, Globals::screenWidth*0.8, Globals::screenHeight/2);
	uiElements.push_back(&animationUI);

	animName.text = "Animation: " + currentAnim->name;
	animName.font = Globals::primaryFont;
	animName.uiRegion.x = 10;
	animName.uiRegion.y = 10;
	animName.generateText();
	uiElements.push_back(&animName);

	addFrameBtn.setup("Add Frame+", Globals::primaryFont);
	addFrameBtn.uiRegion.x = 10;
	addFrameBtn.uiRegion.y = 50;
	addFrameBtn.generateButton();
	uiElements.push_back(&addFrameBtn);


	//add all of my frames to the list to look at
	for (list<Frame>::iterator frame = currentAnim->frames.begin(); frame != currentAnim->frames.end(); frame++){
		addRow(&(*frame));
	}
	redoFrameNumbers();

	backBtn.setup("Back");
	backBtn.uiRegion.x = 10;
	backBtn.generateButton();
	backBtn.uiRegion.y = Globals::screenHeight - 10 - backBtn.uiRegion.h;
	uiElements.push_back(&backBtn);

	frameList.setup(10, 100, Globals::screenWidth*0.6, backBtn.uiRegion.y - 20 - 100);
	uiElements.push_back(&frameList);

}
void ManageAnimationScreen::uiUpdates(){
	if (addFrameBtn.clicked){
		addFrameBtn.clicked = false;

		AddFrameScreen addFrameScreen(currentAnim->spriteSheet);
		addFrameScreen.update();

		if (addFrameScreen.useFrame){
			Frame newFrame(currentAnim->spriteSheet, 0, addFrameScreen.editor.box);
			currentAnim->frames.push_back(newFrame);

			addRow(&newFrame);

			redoFrameNumbers();

			animSet->saveAnimationSet();

			animationUI.reset();
		}
	}

	int frameCounter = 0;
	bool reload = false;
	for (list<UIElement*>::iterator ui = frameList.uiList.begin(); ui != frameList.uiList.end();){
		UIRow* row = (UIRow*)(*ui);
		int rowCounter = 0;
		bool deleteHappened = false;
		for (list<UIElement*>::iterator rowUI = row->uiElements.begin(); rowUI != row->uiElements.end(); )
		{ 
			if (rowCounter == 0)
			{
				//its the image, ignore for now I guess?
			}
			else{
				BasicButton* btn = (BasicButton*)(*rowUI);
				if (btn->clicked){
					btn->clicked = false;
					if (rowCounter == 1){
						//edit this frame
						for (list<Frame>::iterator frame = currentAnim->frames.begin(); frame != currentAnim->frames.end(); frame++){
							//find the frame in the list based on the frame number (these start at 0)
							if (frame->frameNumber == frameCounter){
								//if found, edit this frame
								EditFrameScreen editFrameScreen(animSet, &(*frame));
								editFrameScreen.update();
							}
						}
						
						
					}
					else if(rowCounter == 2){
						//delete this frame
						for (list<Frame>::iterator frame = currentAnim->frames.begin(); frame != currentAnim->frames.end(); frame++){
							//find the frame in the list based on the frame number (these start at 0)
							if (frame->frameNumber == frameCounter){
								//if found, delete, otherwise, do nothing (what can ya do? )
								currentAnim->frames.erase(frame);
								animSet->saveAnimationSet();


								row->removeAll(true);
								ui = frameList.uiList.erase(ui);

								redoFrameNumbers();
								animationUI.reset();
								deleteHappened = true;
								break;
							}
						}
					}
					else if (rowCounter == 3 && frameCounter != 0){
						//move up
						for (list<Frame>::iterator frame = currentAnim->frames.begin(); frame != currentAnim->frames.end(); frame++){
							//find the frame in the list based on the frame number (these start at 0)
							if (frame->frameNumber == frameCounter-1){
								//found frame before it that we want to swap with
								frame->frameNumber++;

								//animSet->saveAnimationSet();
							}
							else if (frame->frameNumber == frameCounter){
								//found the actual frame we want to move up
								frame->frameNumber--;
								
								//Now sort these guys
								currentAnim->frames.sort(Frame::sortFrameByFrameNumber);

								animSet->saveAnimationSet();

								//redoFrameNumbers();
								//animationUI.reset();
								//frameList.updateUIPositions();
								reload = true;
								deleteHappened = true;
								
								break;
							}
						}
					}
					else if (rowCounter == 4 && frameCounter < frameList.uiList.size()-1){
						//move down
						for (list<Frame>::iterator frame = currentAnim->frames.begin(); frame != currentAnim->frames.end(); frame++){
							//find the frame in the list based on the frame number (these start at 0)
							if (frame->frameNumber == frameCounter){
								//frame we want to move up
								frame->frameNumber++;

								//animSet->saveAnimationSet();
							}
							else if (frame->frameNumber == frameCounter+1){
								//found the actual frame we want to move up
								frame->frameNumber--;

								//Now sort these guys
								currentAnim->frames.sort(Frame::sortFrameByFrameNumber);

								animSet->saveAnimationSet();

								//redoFrameNumbers();
								//animationUI.reset();
								//frameList.updateUIPositions();
								reload = true;
								deleteHappened = true;

								break;
							}
						}
					}
				}
			}
			if (deleteHappened)
				break;
			else{
				rowCounter++;
				rowUI++;
			}
			
			
		}
		if (!deleteHappened){
			ui++;
			frameCounter++;
		}

	}

	if (reload){
		reloadList();
	}

	if (backBtn.clicked){
		quit = true;
	}
}

void ManageAnimationScreen::addRow(Frame *frame){
	UIRow *row = new UIRow;

	UIImage* image = new UIImage();
	image->setup(frame->spriteSheetPtr, frame->clip);
	row->uiElements.push_back(image);

	BasicButton* editBtn = new BasicButton();
	editBtn->setup("Edit", Globals::smallFont);
	editBtn->generateButton();
	row->uiElements.push_back(editBtn);

	BasicButton* deleteBtn = new BasicButton();
	deleteBtn->setup("Delete", Globals::smallFont);
	deleteBtn->generateButton();
	row->uiElements.push_back(deleteBtn);

	BasicButton* moveUpBtn = new BasicButton();
	moveUpBtn->setup("Up", Globals::smallFont);
	moveUpBtn->generateButton();
	row->uiElements.push_back(moveUpBtn);

	BasicButton* moveDownBtn = new BasicButton();
	moveDownBtn->setup("Down", Globals::smallFont);
	moveDownBtn->generateButton();
	row->uiElements.push_back(moveDownBtn);

	row->updatePositions();

	frameList.uiList.push_back(row);
	
}

void ManageAnimationScreen::redoFrameNumbers(){
	int frameNum = 0;
	for (list<Frame>::iterator frame = currentAnim->frames.begin(); frame != currentAnim->frames.end(); frame++){
		frame->frameNumber = frameNum;

		frameNum++;
	}
}

void ManageAnimationScreen::reloadList(){
	//remove all rows for some dumb reason
	frameList.uiList.clear();
	//and reload em all
	for (list<Frame>::iterator frame = currentAnim->frames.begin(); frame != currentAnim->frames.end(); frame++){
		addRow(&(*frame));
	}
	redoFrameNumbers();
}