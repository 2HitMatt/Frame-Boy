#ifndef UIBUTTON
#define UIBUTTON

#include <string>
#include "uiElement.h"

using namespace std;

//base button class that essentially does not too much
class UIButton: public UIElement{
public:
	SDL_Color buttonTextColour;
	SDL_Color buttonColour;
	SDL_Color buttonSelectedColour;
	SDL_Color buttonBorderColour;
	string text;
	SDL_Texture* buttonTextTexture = NULL;
	TTF_Font *font;

	bool fixedWidth = false;//if we dont want to change size based on text

	~UIButton(){
		if (buttonTextTexture != NULL)
			cleanup(buttonTextTexture);
	}

	virtual void useDefaultColours();

	void setup(string text, TTF_Font* font = Globals::primaryFont);

	virtual void generateButton();

	//override uielements draw
	void draw();

	

};

#endif