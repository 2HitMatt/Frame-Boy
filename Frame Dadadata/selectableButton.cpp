#include "selectableButton.h"

void SelectableButton::onClick(UIMouseEvent &mouseEvent){
	if (UIButton::checkMouseClick(*this, mouseEvent)){
		mouseEvent.mouseEventUsed = true;

		//make this selected
		selected = true;
		//if there is a selectedUIElement already, unselect it
		if (UIElement::selectedUIElement != NULL && UIElement::selectedUIElement != this)
			UIElement::selectedUIElement->selected = false;

		UIElement::selectedUIElement = this;
	}
}