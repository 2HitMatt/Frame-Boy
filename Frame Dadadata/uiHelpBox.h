#ifndef UIHELPBOX
#define UIHELPBOX

#include "uiList.h"
#include "uiText.h"

class UIHelpBox : public UIElement{
public:
	UIList info;
	int padding = 10;

	~UIHelpBox();
	void onClick(UIMouseEvent &mouseEvent);
	void onDrag(UIMouseEvent &mouseEvent);
	void onRelease(UIMouseEvent &mouseEvent);
	
	void setup(int x, int y, int w, int h, int depth = 1000);
	void addRowOfText(string str);
	void updateHeight();//makes this outside container stretch to fit the list of info bits
	void update();
	void draw();

};

#endif