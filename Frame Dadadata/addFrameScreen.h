#ifndef ADDFRAMESCREEN
#define ADDFRAMESCREEN

#include "uiScreen.h"
#include "uiImageEditor.h"
#include "basicButton.h"
#include "toggleButton.h"
#include "uiHelpBox.h"

class AddFrameScreen : public UIScreen{
public:
	SDL_Texture* texture;

	UIImageEditor editor;

	BasicButton cancelButton, saveButton;
	ToggleButton selectFrameButton;

	UIHelpBox help;

	bool useFrame = false;

	AddFrameScreen(SDL_Texture* texture);
	void uiUpdates();
};

#endif