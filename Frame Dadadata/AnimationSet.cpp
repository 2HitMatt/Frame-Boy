#include "AnimationSet.h"
#include "res_path.h"
#include "drawing_functions.h"
#include "globals.h"
#include "cleanup.h"


AnimationSet::AnimationSet(string outputFileName, string imageName){
	this->outputFileName = outputFileName;
	this->imageName = imageName;
}

	//- takes a file name to load up a bunch of info from, thus creating all of the animations and frames.
AnimationSet::~AnimationSet()
{
	//clean up spritesheet
	cleanup(spriteSheet);

}

void AnimationSet::loadAnimationSet(string fileName){
	outputFileName = fileName;

	ifstream file;
	string resPath = getResourcePath();
	file.open(resPath + "frames/"+fileName);
	if (file.good())
	{
		getline(file, imageName);
		spriteSheet = loadTexture(resPath + "frames/" + imageName, Globals::renderer);

		string buffer;
		getline(file, buffer);
		stringstream ss;
		buffer = Globals::clipOffDataHeader(buffer);
		ss << buffer;
		int numberOfAnimations;
		ss >> numberOfAnimations;

		for (int i = 0; i < numberOfAnimations; i++)
		{
			Animation newAnimation(spriteSheet);
			newAnimation.loadAnimation(file);
			animations.push_back(newAnimation);
		}

	}
	file.close();

	
}
void AnimationSet::saveAnimationSet(){
	ofstream file;
	string resPath = getResourcePath();
	file.open(resPath + "frames/" + outputFileName);

	file << imageName << endl;
	file << "animations: " << animations.size() << endl;
	for (list<Animation>::iterator anim = animations.begin(); anim != animations.end(); anim++){
		(*anim).saveAnimation(file);
	}
	file.close();
}

void AnimationSet::exportToJSON(){
	ofstream file;
	string resPath = getResourcePath();
	file.open(resPath + "frames/" + Globals::getNameFromFile(outputFileName)+".json");
	file << "{" << Globals::jsonKeyValuePairOutput("image", imageName) << endl;
	file << ", \"animations\":[" << endl;
	for (list<Animation>::iterator anim = animations.begin(); anim != animations.end(); anim++){
		(*anim).exportToJSON(file);
		

		list < Animation>::iterator tmp = anim;
		tmp++;
		if (tmp != animations.end())
			file << "," << endl;
	}
	file << "]}";
	file.close();

}

//- properly cleans up spriteSheet
Animation* AnimationSet::getAnimation(string name)
{
	//this->name = name;

	list<Animation>::iterator i;

	//int counter = 0;

	for (i = animations.begin(); i != animations.end(); i++)
	{

		Animation *anim = &*i;

		if (name == anim->name)
		{
			return anim;

		}

		
	}

	return NULL;
}
