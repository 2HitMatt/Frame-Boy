#include "Frame.h"

Frame::Frame(SDL_Texture *spriteSheetPtr){
	this->spriteSheetPtr = spriteSheetPtr;

	GroupBuilder::buildGroups(Globals::currentRuleset.groups, frameData);
}
Frame::Frame(SDL_Texture *spriteSheetPtr, int frameNumber, SDL_Rect clip){
	this->spriteSheetPtr = spriteSheetPtr;
	this->frameNumber = frameNumber;
	this->clip = clip;
	duration = 0.5;

	GroupBuilder::buildGroups(Globals::currentRuleset.groups, frameData);
}


//Draw clip at x - xOffset, y - yOffset either flipped or unflipped. Good luck :P
void Frame::Draw(bool direction, float x, float y)
{
	SDL_Rect dest;
	SDL_RendererFlip flipFlag = SDL_FLIP_NONE;
	dest.y = y;
	dest.x = x;
	dest.w = clip.w;
	dest.h = clip.h;
	
	SDL_RenderCopyEx(Globals::renderer, spriteSheetPtr, &clip, &dest, 0, NULL, flipFlag);
	//deref once for spriteSheet as its a pointer to a pointer
	
}
void Frame::DrawRectangles(list<SDL_Rect> &rects, float x, float y, bool direction)
{
	for (list<SDL_Rect>::iterator it = rects.begin(); it != rects.end(); it++)
	{
		SDL_Rect drawRect = (*it);
		drawRect.x = x + (*it).x;
		drawRect.y = y + (*it).y;
		SDL_RenderDrawRect(Globals::renderer, &drawRect);
	}
}

void Frame::loadFrame(ifstream &file){
	string buffer;
	//frame - dud data saying this is a frame
	getline(file, buffer);
	//clip
	getline(file, buffer);
	stringstream ss;
	buffer = Globals::clipOffDataHeader(buffer);
	ss << buffer;
	ss >> clip.x >> clip.y >> clip.w >> clip.h;
	//offset/pivot
	getline(file, buffer);
	ss.clear();
	buffer = Globals::clipOffDataHeader(buffer);
	ss << buffer;
	ss >> offSet.x >> offSet.y;
	//duration
	getline(file, buffer);
	ss.clear();
	buffer = Globals::clipOffDataHeader(buffer);
	ss << buffer;
	ss >> duration;
	//index
	getline(file, buffer);
	ss.clear();
	buffer = Globals::clipOffDataHeader(buffer);
	ss << buffer;
	ss >> frameNumber;

	GroupBuilder::loadGroups(file, frameData);
}
void Frame::saveFrame(ofstream &file){
	//depends on ruleset really. When I make a more specific one later to deal with texture packer, this will all change again
	
	//dud data just to differentiate frames from each other
	file << "frame"<<endl;
	file << "clip: ";
	Globals::outputSDLRectToStream(file, clip);
	file << "offset: " << offSet.x << " " << offSet.y << endl;
	file << "duration: " << duration << endl;
	file << "index: " << frameNumber << endl;
	
	GroupBuilder::saveGroups(file, frameData);
}

void Frame::exportToJSON(ofstream &file){
	file << "{";
	file << "\"clip\":{" << Globals::jsonBoxOutput(clip) << "},";
	file << "\"offset\":{" << Globals::jsonPointOutput(offSet) << "},";
	file << Globals::jsonKeyValuePairOutput("duration", duration) <<",";
	file << Globals::jsonKeyValuePairOutput("index", frameNumber);
	
	//TODO
	if (frameData.size() > 0){
		file << ",";
		GroupBuilder::exportToJSON(file, frameData);
	}
	file << "}";
}

