#ifndef ADDGROUPSCREEN
#define ADDGROUPSCREEN

#include "uiScreen.h"
#include "uiTextInput.h"
#include "basicButton.h"
#include "uiRadioGroup.h"
#include "uiCheckbox.h"
#include "dataGroupType.h"

class AddGroupScreen : public UIScreen{
public:
	DataGroupType group;
	bool cancelled;

	UITextInput groupNameInput;

	//radiogroup
	UIRadioGroup radioGroup;
	UICheckbox stringGroup;
	UICheckbox pointGroup;
	UICheckbox boxGroup;
	UICheckbox rotationGroup;
	


	UICheckbox singleItemCheckBox;

	BasicButton addButton;
	BasicButton cancelButton;
	

	AddGroupScreen();
	void uiUpdates();

};


#endif