#ifndef GROUPBUILDER
#define GROUPBUILDER

#include "globals.h"
#include "group.h"
#include "groupBox.h"
#include "groupPosition.h"
#include "groupRotation.h"
#include "groupString.h"

class GroupBuilder
{
public:
	/**
	builds a Group based on dataType. default is STRING (because we may want to ignore some data in a set, but still load it)
	*/
	static Group* buildGroup(DataGroupType dataType);
	/**
	Builds a set of Groups based on dataGroupTypes list
	*/
	static void buildGroups(list<DataGroupType> groupTypes, list<Group*> &groups);

	/**
	Add GroupString to groups (build on the fly). Gives back a reference, if you need one
	*/
	static Group* addGroupStringToGroup(string name, list<Group*> &groups);
	/**
	Saves a group out to a file
	*/
	static void saveGroups(ofstream &file, list<Group*> &groups);
	/**
	Exports out to a JSON file
	*/
	static void exportToJSON(ofstream &file, list<Group*> &groups);
	/**
	Loads a group out to a file
	*/
	static void loadGroups(ifstream &file, list<Group*> &groups);
	/**
	Finds a group based on name. Can give back a null
	*/
	static Group* findGroupByName(string str, list<Group*> &groups);

};

#endif