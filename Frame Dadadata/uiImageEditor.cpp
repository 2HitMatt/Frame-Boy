#include "uiImageEditor.h"

const int UIImageEditor::EDITORMODE_MOVE = 1, UIImageEditor::EDITORMODE_POINT = 2, UIImageEditor::EDITORMODE_BOX = 3;

void UIImageEditor::setup(SDL_Texture* texture, SDL_Rect clip){
	this->texture = texture;
	this->clip = clip;
	depth = -10;
	xOffset = 0;
	yOffset = 0;

}
void UIImageEditor::update(){
	//need nuthin here?
}
void UIImageEditor::draw(){
	SDL_Rect dst = { xOffset, yOffset, clip.w*zoom, clip.h*zoom };
	renderTexture(texture, Globals::renderer, dst, &clip);

	SDL_SetRenderDrawColor(Globals::renderer, 255, 255, 255, SDL_ALPHA_OPAQUE);

	if (drawPoint && editorMode == EDITORMODE_POINT){
		SDL_Rect drawPoint = { xOffset + point.x*zoom - 1, yOffset+point.y*zoom - 1, 2 * zoom, 2 * zoom };
		SDL_RenderFillRect(Globals::renderer, &drawPoint);
	}
	if (drawBox && editorMode == EDITORMODE_BOX){
		SDL_Rect drawBox = { xOffset+box.x*zoom, yOffset+box.y*zoom, box.w*zoom, box.h*zoom };
		SDL_RenderDrawRect(Globals::renderer, &drawBox);
	}
}

void UIImageEditor::onClick(UIMouseEvent &mouseEvent){
	if (UIElement::checkMouseClick(*this, mouseEvent)){
		if (editorMode == EDITORMODE_POINT){
			point.x = (mouseEvent.x - xOffset)/zoom;
			point.y = (mouseEvent.y - yOffset)/zoom;
			drawPoint = true;
		}
		if (editorMode == EDITORMODE_BOX){
			box.x = (mouseEvent.x - xOffset)/zoom;
			box.y = (mouseEvent.y - yOffset)/zoom;
			box.w = 1;
			box.h = 1;
			drawBox = true;
		}

		mouseEvent.mouseEventUsed = true;
	}
}
void UIImageEditor::onDrag(UIMouseEvent &mouseEvent){
	if (UIElement::checkMouseClick(*this, mouseEvent, UIMouseEvent::MOUSE_LEFTBUTTON, UIMouseEvent::MOUSE_DRAG)){
		if (editorMode == EDITORMODE_BOX && drawBox){
			box.w = (mouseEvent.x - xOffset) / zoom - box.x;
			box.h = (mouseEvent.y - yOffset) / zoom - box.y;
		}
		if (editorMode == EDITORMODE_MOVE){
			yOffset += mouseEvent.yRel;
			xOffset += mouseEvent.xRel;
		}
		mouseEvent.mouseEventUsed = true;
	}
}
void UIImageEditor::onRelease(UIMouseEvent &mouseEvent){
	if (UIElement::checkMouseClick(*this, mouseEvent, UIMouseEvent::MOUSE_LEFTBUTTON, UIMouseEvent::MOUSE_RELEASE)){
		if (editorMode == EDITORMODE_BOX && drawBox){
			box.w = (mouseEvent.x - xOffset) / zoom - box.x;
			box.h = (mouseEvent.y - yOffset) / zoom - box.y;

			if (box.w < 0){
				box.w = box.w*-1;//make positive
				box.x -= box.w; //shift x to start on the left hand side
			}
			if (box.h < 0){
				box.h = box.h*-1;//make positive
				box.y -= box.h; //shift x to start on the left hand side
			}
		}
		
		mouseEvent.mouseEventUsed = true;
	}
}
void UIImageEditor::onKeyPressed(SDL_Keycode key){
	if (editorMode == UIImageEditor::EDITORMODE_MOVE){
		//zoom in
		if (key == SDLK_z || key == SDLK_PLUS){
			if (zoom < zoomMax)
				zoom++;
		}
		if (key == SDLK_x || key == SDLK_MINUS){
			if (zoom > 1)
				zoom--;
		}
		//move around
		if ( key == SDLK_UP){//key == SDLK_w ||
			yOffset -= 5;
		}
		if ( key == SDLK_DOWN){//key == SDLK_s ||
			yOffset += 5;
		}
		if ( key == SDLK_LEFT){//key == SDLK_a ||
			xOffset -= 5;
		}
		if ( key == SDLK_RIGHT){//key == SDLK_c ||
			xOffset += 5;
		}
	}
	//for testing
	if (key == SDLK_1)
		editorMode = EDITORMODE_MOVE;
	if (key == SDLK_2)
		editorMode = EDITORMODE_POINT;
	if (key == SDLK_3)
		editorMode = EDITORMODE_BOX;
}