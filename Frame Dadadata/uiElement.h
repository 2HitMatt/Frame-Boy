#ifndef UIELEMENT
#define UIELEMENT

#include "drawing_functions.h"
#include "globals.h"
#include "uiMouseEvent.h"

//basic description of a ui element, WOO
class UIElement{
public:
	SDL_Rect uiRegion; //position and size of element
	int depth = 0; //sort these fuckers based on depth, the higher the number, the closer to the l(user)
	bool active = true; //whether this element is on or off (mostly useful for deleting or hiding elements)

	int type; //i'll build some basic type statics so we can cast these bastards back to their full type, if necessary, probs not
	bool selected; //if its the current active item

	virtual void onClick(UIMouseEvent &mouseEvent){ /*override me!*/; }
	virtual void onDrag(UIMouseEvent &mouseEvent){ /*override me!*/; }
	virtual void onRelease(UIMouseEvent &mouseEvent){ /*override me!*/; }
	virtual void onTyped(char input){ /*override me!*/; }
	virtual void onKeyPressed(SDL_Keycode key){/* override me!*/ }
	virtual void onBackSpace(){/*override me*/ }
	virtual void copy(){/**/ }
	virtual void paste(){/**/ }
	virtual void update(){ /*override me!*/; }
	virtual void draw(){ /*override me!*/; }

	static bool touchesUIElement(UIElement &ui, int x, int y){
		if (x >= ui.uiRegion.x && x <= ui.uiRegion.x + ui.uiRegion.w &&
			y >= ui.uiRegion.y && y <= ui.uiRegion.y + ui.uiRegion.h)
			return true;

		return false;
	}

	//helper
	static bool checkMouseClick(UIElement &button, UIMouseEvent &mouseEvent, int buttonClick = UIMouseEvent::MOUSE_LEFTBUTTON, int type = UIMouseEvent::MOUSE_CLICK){
		if (!mouseEvent.mouseEventUsed && //not used
			mouseEvent.type == type && //is a click
			mouseEvent.button == buttonClick &&  //is left button
			UIElement::touchesUIElement(button, mouseEvent.x, mouseEvent.y)) //and clicked this button
			return true;

		return false;
	}

	//for sorting
	static bool UIElementCompareDepthForDraw(const UIElement*  a, const UIElement*  b)
	{
		//reverse if not stacked properly
		return a->depth < b->depth;

	}

	static bool UIElementCompareDepthForInteraction(const UIElement*   a, const UIElement*   b)
	{
		//reverse if not stacked properly
		return a->depth > b->depth;

	}

	static UIElement* selectedUIElement;
};


#endif