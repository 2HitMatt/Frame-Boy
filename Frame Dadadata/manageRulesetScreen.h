#ifndef MANAGERULESETSCREEN
#define MANAGERULESETSCREEN

#include "uiScreen.h"
#include "uiList.h"
#include "uiText.h"
#include "uiRow.h"
#include "basicButton.h"

//list all rulesets. delete those we dont need(maybe), load up one we want to be current
class ManageRulesetScreen : public UIScreen{
	public:
		UIText label;
		list<string> rules;
		UIList rulesets;
		BasicButton backButton;

		ManageRulesetScreen();
		~ManageRulesetScreen(){
			rulesets.removeAll(true);
		}
		void uiUpdates();
};

#endif
