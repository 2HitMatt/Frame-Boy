#ifndef EDITFRAMESCREEN
#define EDITFRAMESCREEN

//THIS IS THE SCREEN WE'VE BEEN AIMING FOR
//ADD YOUR FRAME DATAS HERE

#include "uiScreen.h"
#include "uiImageEditor.h"
#include "uiList.h"
#include "uiTextInput.h"
#include "uiText.h"
#include "toggleButton.h"
#include "AnimationSet.h"
#include "uiFrameDataGroup.h"
#include "uiHelpBox.h"

class EditFrameScreen : public UIScreen{
public:
	static const int EF_NO_ONE, EF_OFFSET, EF_OTHER_DATA;
	
	AnimationSet *animSet;
	Frame *frame;

	int edittingFor = EF_NO_ONE;

	UIImageEditor editor;

	UIList dataPanel; //all of our data related items are in this panel, hopefully easy :/
	list<UIFrameDataGroup*> frameDataElements; //I'll work this out asap

	//known data elements
	UITextInput durationInput;
	ToggleButton offsetButton;

	BasicButton cancelButton, saveButton;

	UIHelpBox help;

	EditFrameScreen(AnimationSet *animSet, Frame *frame);
	void uiUpdates();
};

#endif