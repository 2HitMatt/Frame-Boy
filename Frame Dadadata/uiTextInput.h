#ifndef UITEXTINPUT
#define UITEXTINPUT

#include "uiButton.h"

class UITextInput : public UIButton{
public:
	
	
	string inputText;
	int inputMax; //also using this as a whitespace counter to help plan out the button size
	SDL_Texture* inputTextTexture = NULL;
	bool numbersOnly = false;

	void generateButton();

	void updateText();

	void onClick(UIMouseEvent &mouseEvent);
	void onTyped(char input);
	void onBackSpace();
	void copy();
	void paste();
	void useDefaultColours();
	void draw();

private:
	int introTextWidth = 0; //mostly a helper
};

#endif