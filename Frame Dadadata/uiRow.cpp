#include "uiRow.h"

UIRow::UIRow(){
	rowColour = { 0xFF, 0xFF, 0xFF, 0xFF };
}

void UIRow::onClick(UIMouseEvent &mouseEvent){
	for (list<UIElement*>::iterator uiE = uiElements.begin(); uiE != uiElements.end(); uiE++){
		(*uiE)->onClick(mouseEvent);
	}
}
void UIRow::onDrag(UIMouseEvent &mouseEvent){
	for (list<UIElement*>::iterator uiE = uiElements.begin(); uiE != uiElements.end(); uiE++){
		(*uiE)->onDrag(mouseEvent);
	}
}
void UIRow::onRelease(UIMouseEvent &mouseEvent){
	for (list<UIElement*>::iterator uiE = uiElements.begin(); uiE != uiElements.end(); uiE++){
		(*uiE)->onRelease(mouseEvent);
	}
}

void UIRow::updatePositions(){
	int accumulativeWidth = 0;//as we find more elements, keep adding their widths onto this so we can work out the next elements y value
	int biggestHeight = 0;
	for (list<UIElement*>::iterator uiE = uiElements.begin(); uiE != uiElements.end(); uiE++){
		(*uiE)->uiRegion.y = uiRegion.y;
		(*uiE)->uiRegion.x = uiRegion.x + accumulativeWidth;
		accumulativeWidth += (*uiE)->uiRegion.w;
		if ((*uiE)->uiRegion.h > biggestHeight){
			biggestHeight = (*uiE)->uiRegion.h;

		}
	}
	uiRegion.h = biggestHeight;
	uiRegion.w = accumulativeWidth;
}
void UIRow::draw(){
	updatePositions();

	depth = -50; //low depth

	SDL_SetRenderDrawColor(Globals::renderer, rowColour.r, rowColour.g, rowColour.g, rowColour.a);
	SDL_RenderFillRect(Globals::renderer, &uiRegion);

	for (list<UIElement*>::iterator uiE = uiElements.begin(); uiE != uiElements.end(); uiE++){
		(*uiE)->update();
		(*uiE)->draw();
	}
}

void UIRow::removeInActive(bool deleteThem){
	for (list<UIElement*>::iterator uiE = uiElements.begin(); uiE != uiElements.end(); ){
		if (!(*uiE)->active){
			if (deleteThem)
				delete (*uiE);
			uiE = uiElements.erase(uiE);
		}
		else
		{
			uiE++;
		}
	}
}

void UIRow::removeAll(bool deleteThem){
	for (list<UIElement*>::iterator uiE = uiElements.begin(); uiE != uiElements.end();){		
		if (deleteThem)
			delete (*uiE);
		uiE = uiElements.erase(uiE);
		
	}
}