#ifndef Animation_header
#define Animation_header

#include <iostream>
#include <list>
#include "SDL.h"
#include "globals.h"
#include "Frame.h"

using namespace std;

class Animation
{
public:
	string name; //name of the animation, used to find and load up
	list<Frame> frames; //list of our frames
	SDL_Texture *spriteSheet; //pointer to our spritesheet
	
	//Functions:
	Animation(SDL_Texture *spriteSheet, string name = "");
	
	void Draw(bool direction, float x, float y); //� calls the current frames draw
	int getNextFrameNumber(int frameNumber); //� returns the next Frame in the list
	Frame* getNextFrame(Frame* frame);
	int getEndFrameNumber(); //- returns the final frame
	Frame* getFrame(int frameNumber);

	//save/load
	void loadAnimation(ifstream &file); //note: need to pass spritesheet through so frames can get the ref
	void saveAnimation(ofstream &file);
	void exportToJSON(ofstream &file);
};

#endif