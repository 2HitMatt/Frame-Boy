#include "toggleButton.h"

void ToggleButton::onClick(UIMouseEvent &mouseEvent){
	if (UIButton::checkMouseClick(*this, mouseEvent)){
		mouseEvent.mouseEventUsed = true;

		selected = !selected; //toggle its selected-ness

		if (selectGlobally){
			//deselect previous
			if (UIElement::selectedUIElement != NULL && UIElement::selectedUIElement != this)
				UIElement::selectedUIElement->selected = false;
			
			if (selected){	
				//im the new selected element
				UIElement::selectedUIElement = this;
			}
			else{
				//im no longer the selected element
				UIElement::selectedUIElement = NULL;
			}
		}

		clicked = true;
	}
}