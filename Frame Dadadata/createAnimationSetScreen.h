#ifndef CREATEANIMATIONSETSCREEN
#define CREATEANIMATIONSETSCREEN

#include "uiScreen.h"
#include "uiList.h"
#include "uiTextInput.h"
#include "selectableButton.h"
#include "uiText.h"
#include "AnimationSet.h"
#include "uiHelpBox.h"

class CreateAnimationSetScreen : public UIScreen
{
public:
	UITextInput nameInput;

	UIText imageText;
	string selectedImageFile = "";
	list<string> images;
	UIList imageFilesList;

	BasicButton saveButton;
	BasicButton cancelButton;

	UIHelpBox help;

	string newAnimSetFile = "";

	CreateAnimationSetScreen();
	void uiUpdates();
};

#endif