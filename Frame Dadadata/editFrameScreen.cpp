#include "editFrameScreen.h"

const int EditFrameScreen::EF_NO_ONE = 0, EditFrameScreen::EF_OFFSET = 1,  EditFrameScreen::EF_OTHER_DATA = 2;

EditFrameScreen::EditFrameScreen(AnimationSet *animSet, Frame *frame){
	UIElement::selectedUIElement = NULL;
	
	this->animSet = animSet;
	this->frame = frame;

	editor.setup(frame->spriteSheetPtr, frame->clip);
	editor.uiRegion.x = 0;
	editor.uiRegion.y = 0;
	editor.uiRegion.w = Globals::screenWidth;
	editor.uiRegion.h = Globals::screenHeight;
	uiElements.push_back(&editor);
	keyPressElements.push_back(&editor);

	cancelButton.setup("Cancel");
	cancelButton.generateButton();
	cancelButton.uiRegion.x = 10;
	cancelButton.uiRegion.y = Globals::screenHeight - cancelButton.uiRegion.h - 2;
	uiElements.push_back(&cancelButton);

	saveButton.setup("Save");
	saveButton.generateButton();
	saveButton.uiRegion.x = cancelButton.uiRegion.x + cancelButton.uiRegion.w + 5;
	saveButton.uiRegion.y = Globals::screenHeight - saveButton.uiRegion.h - 2;
	uiElements.push_back(&saveButton);

	dataPanel.setup(Globals::screenWidth - (Globals::screenWidth*0.4), 0, Globals::screenWidth*0.4, Globals::screenHeight);
	dataPanel.depth = 10;
	uiElements.push_back(&dataPanel);

	//frame has clip, offset, duration and frame number. So we need to cater for offset and duration first
	durationInput.setup("Duration: ");
	durationInput.inputMax = 4;
	durationInput.numbersOnly = true;
	durationInput.generateButton();
	stringstream ss;
	ss << frame->duration;
	durationInput.inputText = ss.str();
	durationInput.updateText();
	dataPanel.uiList.push_back(&durationInput);

	ss.clear();
	ss.str("");
	ss << "Offset: " << frame->offSet.x << " " << frame->offSet.y;
	offsetButton.setup(ss.str());
	offsetButton.generateButton();
	offsetButton.selectGlobally = true;
	dataPanel.uiList.push_back(&offsetButton);

	for (list<Group*>::iterator group = frame->frameData.begin(); group != frame->frameData.end(); group++){
		UIFrameDataGroup *fdg = new UIFrameDataGroup(*group, &editor);
		fdg->uiRegion.w = dataPanel.uiRegion.w;
		dataPanel.updateUIPositions();
		fdg->updatePositions();
		dataPanel.uiList.push_back(fdg);
	}
	
	help.setup(10, saveButton.uiRegion.y - 210, 600, 200);
	help.addRowOfText("Buttons: Z - zoom in, X - zoom out, Directional Arrows to move canvas");
	help.addRowOfText("Editor Move Mode: Drag canvas around to move");
	help.addRowOfText("Box Select Mode: Click and Drag the clip region to use for the box");
	help.addRowOfText("Point Select Mode: Click on canvas to select a point");
	help.addRowOfText(" ");
	help.addRowOfText("Guide: select offset, set duration, add data to groups.");
	help.addRowOfText("Edit the data in the groups. Some use the canvas, some require input");

	uiElements.push_back(&help);

}
void EditFrameScreen::uiUpdates(){
	
	dataPanel.updateUIPositions();


	if (offsetButton.clicked){
		offsetButton.clicked = false;
		if (offsetButton.selected){
			editor.drawPoint = true;
			editor.point = frame->offSet;
			edittingFor = EF_OFFSET;
			editor.editorMode = UIImageEditor::EDITORMODE_POINT;
		}
		else{
			edittingFor = EF_NO_ONE;
			editor.editorMode = UIImageEditor::EDITORMODE_MOVE;
		}
	}
	
	if (UIElement::selectedUIElement == &offsetButton && offsetButton.selected)
	{
		if (editor.drawPoint){
			frame->offSet = editor.point;

			stringstream ss;
			ss << "Offset: " << frame->offSet.x << " " << frame->offSet.y;
			offsetButton.text=ss.str();
			offsetButton.generateButton();
		}
	}
	else if (UIElement::selectedUIElement == NULL){
		edittingFor = EF_NO_ONE;
		editor.editorMode = UIImageEditor::EDITORMODE_MOVE;
	}

	if (cancelButton.clicked){
		quit = true;
	}
	if (saveButton.clicked){
		stringstream ss;
		ss << durationInput.inputText;
		ss >>frame->duration;
		animSet->saveAnimationSet();
		quit = true;
		//plus way more
	}
}