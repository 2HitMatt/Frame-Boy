#ifndef UIIMAGE
#define UIIMAGE

#include "drawing_functions.h"
#include "uiElement.h"

class UIImage : public UIElement{
public:
	SDL_Texture* texture;
	SDL_Rect clip;

	void setup(SDL_Texture* texture, SDL_Rect clip);
	void draw();
};

#endif