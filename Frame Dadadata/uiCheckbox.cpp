#include "uiCheckbox.h"

void UICheckbox::generateButton(){
	buttonTextTexture = renderText(text, font, buttonColour, Globals::renderer);
	checkboxCheckedTexture = renderText("X", font, buttonTextColour, Globals::renderer);
	//work out new button size
	int W, H;
	SDL_QueryTexture(checkboxCheckedTexture, NULL, NULL, &W, &H);
	uiRegion.w = W + 4;
	uiRegion.h = H + 4;
}

void UICheckbox::onClick(UIMouseEvent &mouseEvent){
	if (UIButton::checkMouseClick(*this, mouseEvent)){
		mouseEvent.mouseEventUsed = true;
		checked = !checked;
	}
}

void UICheckbox::draw(){
	if (buttonTextTexture == NULL)
		generateButton();

	if (selected)
		SDL_SetRenderDrawColor(Globals::renderer, buttonSelectedColour.r, buttonSelectedColour.g, buttonSelectedColour.b, SDL_ALPHA_OPAQUE);
	else
		SDL_SetRenderDrawColor(Globals::renderer, buttonColour.r, buttonColour.g, buttonColour.b, SDL_ALPHA_OPAQUE);
	SDL_RenderFillRect(Globals::renderer, &uiRegion);

	SDL_SetRenderDrawColor(Globals::renderer, buttonBorderColour.r, buttonBorderColour.g, buttonBorderColour.b, SDL_ALPHA_OPAQUE);
	SDL_RenderDrawRect(Globals::renderer, &uiRegion);

	//currently checkboxes on the left

	if (checked)
		renderTexture(checkboxCheckedTexture, Globals::renderer, uiRegion.x + 2, uiRegion.y + 2);

	renderTexture(buttonTextTexture, Globals::renderer, uiRegion.x + uiRegion.w + 2, uiRegion.y + 2);
}