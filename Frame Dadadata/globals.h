#ifndef GLOBALS_H
#define GLOBALS_H

#include <string>
#include <iostream>
#include <SDL.h>
#include <SDL_ttf.h>
#include "randomNumber.h"
#include "configFile.h"
#include "ruleset.h"
#include "dirent.h"

using namespace std;

//http://www.learncpp.com/cpp-tutorial/811-static-member-variables/
class Globals
{
public:
	//for calculations
	static const double PI ;
	
	//for game stuff
	static ConfigFile configFile;
	static Ruleset currentRuleset;
	static int screenWidth;
	static int screenHeight;
	static bool showHelp;

	static Uint32 currentTicks;
	static Uint32 pausedTicks;
	static SDL_Rect *camera;
	static SDL_Renderer* renderer;

	static TTF_Font *primaryFont;
	static TTF_Font *secondaryFont;
	static TTF_Font *smallFont;

	//game functions
	static void updateTicks();


	//useful functions
	static string getNameFromFile(string f);
	static void filesInADir(list<string> &files, string dir, string ext = "");
	static string jsonKeyValuePairOutput(string key, string value);
	static string jsonKeyValuePairOutput(string key, int value);
	static string jsonKeyValuePairOutput(string key, float value);
	static string jsonBoxOutput(SDL_Rect box);
	static string jsonPointOutput(SDL_Point point);

	//sdl helpers
	static int SDL_ToggleFS(SDL_Window *win, SDL_Renderer *render);
	static void outputSDLRectToStream(ostream &stream, SDL_Rect r);
	/**cuts off the extra info. e.g hitbox: 2 23 23 23, it cuts off the 'hitbox: ' part and just leaves the numbers*/
	static string clipOffDataHeader(string str);
	
};

#endif