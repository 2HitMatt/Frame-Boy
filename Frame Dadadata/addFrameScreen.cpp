#include "addFrameScreen.h"

AddFrameScreen::AddFrameScreen(SDL_Texture* texture){
	UIElement::selectedUIElement = NULL;

	this->texture = texture;

	//clip is the entire thing
	SDL_Rect clip = { 0, 0, 0, 0 };
	SDL_QueryTexture(texture, NULL, NULL, &clip.w, &clip.h);

	editor.setup(texture, clip);
	editor.uiRegion.x = 0;
	editor.uiRegion.y = 0;
	editor.uiRegion.w = Globals::screenWidth;
	editor.uiRegion.h = Globals::screenHeight;
	uiElements.push_back(&editor);
	keyPressElements.push_back(&editor);

	cancelButton.setup("Cancel");
	cancelButton.generateButton();
	cancelButton.uiRegion.x = 10;
	cancelButton.uiRegion.y = Globals::screenHeight - cancelButton.uiRegion.h - 2;
	uiElements.push_back(&cancelButton);

	saveButton.setup("Save");
	saveButton.generateButton();
	saveButton.uiRegion.x = cancelButton.uiRegion.x + cancelButton.uiRegion.w + 5;
	saveButton.uiRegion.y = Globals::screenHeight - saveButton.uiRegion.h - 2;
	uiElements.push_back(&saveButton);

	selectFrameButton.setup("Select Frame Region");
	selectFrameButton.generateButton();
	selectFrameButton.uiRegion.x = saveButton.uiRegion.x + saveButton.uiRegion.w + 5;
	selectFrameButton.uiRegion.y = Globals::screenHeight - selectFrameButton.uiRegion.h - 2;
	uiElements.push_back(&selectFrameButton);

	help.setup(10, selectFrameButton.uiRegion.y - 110, 600, 100);
	help.addRowOfText("Buttons: Z - zoom in, X - zoom out, Directional Arrows to move canvas");
	help.addRowOfText("Editor Move Mode: Drag canvas around to move");
	help.addRowOfText("Box Select Mode: Click and Drag the clip region to use for the frame");
	uiElements.push_back(&help);

	
}
void AddFrameScreen::uiUpdates(){
	if (cancelButton.clicked){
		UIElement::selectedUIElement = NULL;
		quit = true;
	}
	if (saveButton.clicked && editor.drawBox){
		useFrame = true;
		UIElement::selectedUIElement = NULL;
		quit = true;
	}
	else{
		saveButton.clicked = false;
	}
	
	if (selectFrameButton.selected){
		editor.editorMode = UIImageEditor::EDITORMODE_BOX;
	}
	else{
		editor.editorMode = UIImageEditor::EDITORMODE_MOVE;
	}
	
}