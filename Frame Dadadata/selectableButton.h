#ifndef SELECTABLEBUTTON
#define SELECTABLEBUTTON

#include "basicButton.h"

class SelectableButton : public BasicButton{
public:

	void onClick(UIMouseEvent &mouseEvent);
};

#endif