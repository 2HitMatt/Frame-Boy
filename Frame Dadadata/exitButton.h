#ifndef EXITBUTTON
#define EXITBUTTON

#include "uiButton.h"

//used to exit program
class ExitButton : public UIButton{
public:
	bool quit = false;
	
	ExitButton();

	void onClick(UIMouseEvent &mouseEvent);

};

#endif