#ifndef DEACTIVATEBUTTON
#define DEACTIVATEBUTTON

#include "uiButton.h"

//when clicked, it de-activates an element
class DeactivateButton : public UIButton{
public:
	UIElement *elementToDeactivate;

	void onClick(UIMouseEvent &mouseEvent);
};

#endif