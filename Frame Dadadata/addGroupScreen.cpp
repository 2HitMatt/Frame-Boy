#include "addGroupScreen.h"


AddGroupScreen::AddGroupScreen(){
	UIElement::selectedUIElement = NULL;

	groupNameInput.setup("Group Name: ", Globals::primaryFont);
	groupNameInput.inputMax = 20;
	groupNameInput.uiRegion.x = 10;
	groupNameInput.uiRegion.y = 10;
	groupNameInput.generateButton();
	uiElements.push_back(&groupNameInput);

	singleItemCheckBox.setup("Single Item Group (holds only 1 bit of data)");
	singleItemCheckBox.uiRegion.x = 10;
	singleItemCheckBox.uiRegion.y = 50;
	singleItemCheckBox.generateButton();
	uiElements.push_back(&singleItemCheckBox);
	
	stringGroup.setup("String/text Group");
	stringGroup.uiRegion.x = 10;
	stringGroup.uiRegion.y = singleItemCheckBox.uiRegion.h + singleItemCheckBox.uiRegion.y + 50;
	stringGroup.generateButton();
	uiElements.push_back(&stringGroup);

	pointGroup.setup("Point/Position Group");
	pointGroup.uiRegion.x = 10;
	pointGroup.uiRegion.y = stringGroup.uiRegion.h + stringGroup.uiRegion.y + 10;
	pointGroup.generateButton();
	uiElements.push_back(&pointGroup);

	boxGroup.setup("Box Group");
	boxGroup.uiRegion.x = 10;
	boxGroup.uiRegion.y = pointGroup.uiRegion.h + pointGroup.uiRegion.y + 10;
	boxGroup.generateButton();
	uiElements.push_back(&boxGroup);

	rotationGroup.setup("Number/Decimal Group");
	rotationGroup.uiRegion.x = 10;
	rotationGroup.uiRegion.y = boxGroup.uiRegion.h + boxGroup.uiRegion.y + 10;
	rotationGroup.generateButton();
	uiElements.push_back(&rotationGroup);

	radioGroup.radioButtons.push_back(&stringGroup);
	radioGroup.radioButtons.push_back(&pointGroup);
	radioGroup.radioButtons.push_back(&boxGroup);
	radioGroup.radioButtons.push_back(&rotationGroup);
	uiElements.push_back(&radioGroup);

	addButton.setup("Add", Globals::primaryFont);
	addButton.uiRegion.x = 10;
	addButton.generateButton();
	addButton.uiRegion.y = Globals::screenHeight - addButton.uiRegion.h - 10;
	uiElements.push_back(&addButton);

	cancelButton.setup("Cancel", Globals::primaryFont);
	cancelButton.uiRegion.x = 300;
	cancelButton.generateButton();
	cancelButton.uiRegion.y = Globals::screenHeight - addButton.uiRegion.h - 10;
	uiElements.push_back(&cancelButton);
}
void AddGroupScreen::uiUpdates(){
	if (addButton.clicked){
		//setup group with this data for use
		group.groupName = groupNameInput.inputText;
		group.singleItem = singleItemCheckBox.checked;

		//check my checkboxes
		if (stringGroup.checked)
			group.dataType = DataGroupType::DATATYPE_STRING;
		if (boxGroup.checked)
			group.dataType = DataGroupType::DATATYPE_BOX;
		if (pointGroup.checked)
			group.dataType = DataGroupType::DATATYPE_POSITION;
		if (rotationGroup.checked)
			group.dataType = DataGroupType::DATATYPE_ROTATION;
		

		UIElement::selectedUIElement = NULL;
		cancelled = false;
		quit = true;
	}
	if (cancelButton.clicked){
		UIElement::selectedUIElement = NULL;
		cancelled = true;
		quit = true;
	}
	


}

