#ifndef GROUPSTRING_H
#define GROUPSTRING_H

#include "group.h"

class GroupString : public Group{
public:
	list<string> data;

	GroupString(DataGroupType type){
		this->type = type;
	}
	int numberOfDataInGroup(){
		return data.size();
	}

	void outputGroup(ostream &stream, bool inGroups){
		for (list <string>::iterator str = data.begin(); str != data.end(); str++){

			if (!inGroups)
				stream << type.groupName << ": ";

			stream << (*str) << endl;
		}


	}

	void exportToJSON(ostream &stream, bool inGroups){
		stream << "\"" << type.groupName << "\":[";
		for (list < string>::iterator r = data.begin(); r != data.end(); r++){
			stream <<"{"<< Globals::jsonKeyValuePairOutput("value", *r)<<"}";
			

			list < string>::iterator tmp = r;
			tmp++;
			if (tmp != data.end())
				stream << "," << endl;

		}
		stream << "]";
	}
	void addToGroup(string str){
		//bah, if we got a whitespace at the front, lets get rid of that
		if (str[0] == ' '){
			str.erase(0, 1);
		}

		data.push_back(str);
	}

	void draw(){
		//TODO
	}

};


#endif