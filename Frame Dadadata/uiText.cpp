#include "uiText.h"

void UIText::generateText(){
	if (textTexture == NULL){
		if (text != "")
			textTexture = renderText(text, font, textColour, Globals::renderer);
		else
			textTexture = renderText(" ", font, textColour, Globals::renderer);

		//work out new button size
		int W, H;
		SDL_QueryTexture(textTexture, NULL, NULL, &W, &H);
		uiRegion.w = W + 4;
		uiRegion.h = H + 4;
	}
}

void UIText::changeText(string text){
	if (textTexture != NULL){
		cleanup(textTexture);
		textTexture = NULL;
		this->text = text;
		//generateText();
	}

	//then it will rebuild texture on next draw
}
//override uielements draw
void UIText::draw(){
	generateText();

	renderTexture(textTexture, Globals::renderer, uiRegion.x+2, uiRegion.y+2);
}