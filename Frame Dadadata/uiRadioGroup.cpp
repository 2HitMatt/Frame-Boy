#include "uiRadioGroup.h"

void UIRadioGroup::update(){
	if (lastChecked == NULL){
		//find first uiCheckBox that is selected and deselect the rest (hopefully happens only once)
		for (list<UICheckbox*>::iterator cb = radioButtons.begin(); cb != radioButtons.end(); cb++){
			if ((*cb)->checked){
				if (lastChecked == NULL)
					lastChecked = (*cb);
				else
					(*cb)->checked = false;
			}
		}
		//if none selected, we'll just set it to the first one
		if (lastChecked == NULL){
			lastChecked = radioButtons.front();
			lastChecked->checked = true;
		}
	}
	
	bool nonChecked = true;
	//if a new checkbox has been selected since last time
	for (list<UICheckbox*>::iterator cb = radioButtons.begin(); cb != radioButtons.end(); cb++){
		if ((*cb)->checked)
			nonChecked = false;
		
		if ((*cb)->checked && (*cb) != lastChecked) //this selected item is different to the last one!
		{
			//unselect the old one
			lastChecked->checked = false;
			//and hold the reference to this newly selected one now
			lastChecked = (*cb);
		}
	}
	//cannot unselect a value in a radio button list
	if (nonChecked && lastChecked != NULL){
		lastChecked->checked = true;
	}

}