#include "globals.h"

const double Globals::PI = 3.14159265;

ConfigFile Globals::configFile;
Ruleset Globals::currentRuleset;
int Globals::screenWidth = 800;
int Globals::screenHeight = 600;
bool Globals::showHelp = false;


Uint32 Globals::currentTicks = 0;
Uint32 Globals::pausedTicks = 0;
SDL_Rect *Globals::camera = 0;
SDL_Renderer *Globals::renderer = 0;

TTF_Font *Globals::primaryFont = NULL;
TTF_Font *Globals::secondaryFont = NULL;
TTF_Font *Globals::smallFont = NULL;


void Globals::updateTicks()
{
	Globals::currentTicks = SDL_GetTicks() - Globals::pausedTicks;
}

string Globals::getNameFromFile(string f)
{
	int position = 0;
	int p = 0;
	//remove all of the junk before the file name
	while (p != -1)
	{
		position = p;
		p++;
		p = f.find("/", p);
	}
	if (position > 0)
		f.erase(0, position + 1);
	//remove the junk after the dot
	position = f.find('.', 0);
	if (position != -1)
	{
		f = f.substr(0, position);

	}

	//string name = "";
	return f;
}


void Globals::filesInADir(list<string> &files, string directory, string ext){

	DIR *dir;
	struct dirent *ent;
	if ((dir = opendir(directory.c_str())) != NULL) {
		/* print all the files and directories within directory */
		while ((ent = readdir(dir)) != NULL) {
			//printf("%s\n", ent->d_name);
			string file = ent->d_name;
			
			//ignore files starting with .
			if (file[0] != '.'){
				//if not worried about file extension, or we are and this file matches it
				if (ext == "" || (ext != "" && file.find(ext,0) != -1))
				files.push_back(file);
			}
		}
		closedir(dir);
	}
	else {
		/* could not open directory */
		perror("");
		//return EXIT_FAILURE;
	}
}

string Globals::jsonKeyValuePairOutput(string key, string value){
	return "\"" + key + "\":\"" + value + "\"";
}
string Globals::jsonKeyValuePairOutput(string key, int value){
	stringstream ss;
	ss << value;
	return jsonKeyValuePairOutput(key, ss.str());
}
string Globals::jsonKeyValuePairOutput(string key, float value){
	stringstream ss;
	ss << value;
	return jsonKeyValuePairOutput(key, ss.str());
}
string Globals::jsonBoxOutput(SDL_Rect box){
	string output = "\"box\":{\n";
	output += jsonKeyValuePairOutput("x", box.x) + "," + jsonKeyValuePairOutput("y", box.y) + ","+
		jsonKeyValuePairOutput("w", box.w) + "," + jsonKeyValuePairOutput("h", box.h) + "}";
	return output;
}
string Globals::jsonPointOutput(SDL_Point point){
	string output = "\"point\":{\n";
	output += jsonKeyValuePairOutput("x", point.x) + "," + jsonKeyValuePairOutput("y", point.y) + "}";
	return output;
}

int Globals::SDL_ToggleFS(SDL_Window *win, SDL_Renderer *render)
{
	Uint32 flags = (SDL_GetWindowFlags(win) ^ SDL_WINDOW_FULLSCREEN_DESKTOP);
	if (SDL_SetWindowFullscreen(win, flags) < 0) // NOTE: this takes FLAGS as the second param, NOT true/false! 
	{
		std::cout << "Toggling fullscreen mode failed: " << SDL_GetError() << std::endl;
		return -1;
	}
	int w = 600, h = 400; // TODO: UPDATE ME 
	if ((flags & SDL_WINDOW_FULLSCREEN_DESKTOP) != 0)
	{
		SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "nearest");
		SDL_RenderSetLogicalSize(render, w, h); // TODO: pass renderer as param maybe? 
		return 1;
	}
	SDL_SetWindowSize(win, w, h);
	return 0;
}

void Globals::outputSDLRectToStream(ostream &stream, SDL_Rect r){
	stream << r.x << " " << r.y << " " << r.w << " " << r.h << endl;
	
}

string Globals::clipOffDataHeader(string str){
	int pos = str.find(":", 0);
	if (pos != -1){
		str = str.substr(pos + 1, str.length() - pos + 2);
	}
	return str;
}