#ifndef GROUPROTATION_H
#define GROUPROTATION_H

#include "group.h"

class GroupRotation : public Group{
public:
	list<float> data;

	GroupRotation(DataGroupType type){
		this->type = type;
	}
	int numberOfDataInGroup(){
		return data.size();
	}

	void outputGroup(ostream &stream, bool inGroups){
		for (list < float>::iterator r = data.begin(); r != data.end(); r++){

			if (!inGroups)
				stream << type.groupName << ": ";

			stream << (*r)<<endl;
		}
	}
	void exportToJSON(ostream &stream, bool inGroups){
		stream << "\"" << type.groupName << "\":[";
		for (list < float>::iterator r = data.begin(); r != data.end(); r++){
			stream <<"{"<< Globals::jsonKeyValuePairOutput("number",*r)<<"}";
		
			list < float>::iterator tmp = r;
			tmp++;
			if (tmp != data.end())
				stream << "," << endl;

		}
		stream << "]";
	}
	void addToGroup(string str){
		//hope this works :/
		stringstream ss;
		ss << str;
		float r;
		ss >>r;

		data.push_back(r);
	}

	void draw(){
		//TODO
	}

};


#endif