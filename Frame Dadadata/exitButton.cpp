#include "exitButton.h"

ExitButton::ExitButton(){
	//setup
	text = "Exit";
	font = Globals::primaryFont;
	useDefaultColours();
}

void ExitButton::onClick(UIMouseEvent &mouseEvent){
	if (UIButton::checkMouseClick(*this, mouseEvent)){
		mouseEvent.mouseEventUsed = true;

		quit = true;
	}
}