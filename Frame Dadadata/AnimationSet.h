#ifndef AnimationSet_header
#define AnimationSet_header

#include <iostream>
#include <fstream>
#include <list>
#include "SDL.h"
#include "globals.h"
#include "Animation.h"

using namespace std;

class AnimationSet
{
public:

	string outputFileName;
	string imageName;
	SDL_Texture *spriteSheet; //this is where we load this one up and then pass it down the line. Meaning here is the class we also have to destroy it in here
	list<Animation> animations;
	//Functions:
	AnimationSet(){ ; } //generic one used for load/save
	AnimationSet(string outputFileName, string imageName); //used for creating a new animationSet
	~AnimationSet(); //- properly cleans up spriteSheet
	
	//save/load
	void loadAnimationSet(string fileName);
	void saveAnimationSet();
	void exportToJSON();
	
	//helpers
	Animation* getAnimation(string name); //� gets an animation by name(can return null if no match)
};

#endif