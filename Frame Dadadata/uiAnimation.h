#ifndef UIANIMATION
#define UIANIMATION

#include "uiImage.h"
#include "Animation.h"
#include "TimeController.h"

class UIAnimation : public UIElement{
public:
	Animation* anim;
	Frame* frame;
	float counter;
	int drawX, drawY;

	void setup(Animation* anim, int x, int y);
	void update();
	void draw();
	void updateUIRegion();
	void reset(){
		frame = anim->getFrame(0);
		counter = 0;
	}
};

#endif