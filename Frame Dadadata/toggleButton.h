#ifndef TOGGLEBUTTON
#define TOGGLEBUTTON

#include "basicButton.h"

class ToggleButton : public BasicButton{
public:
	bool selectGlobally = false; //so a button can be 'selected' but also we can say this is main focus of the program by setting selectedUIElement

	void onClick(UIMouseEvent &mouseEvent);
};

#endif