#include "manageAnimationSetScreen.h"

ManageAnimationSetScreen::ManageAnimationSetScreen(string animationSetFile){
	UIElement::selectedUIElement = NULL;
	
	animSet.loadAnimationSet(animationSetFile);
	animsetName.text = "Animation Set: " + Globals::getNameFromFile(animSet.outputFileName);
	animsetName.uiRegion.x = 10;
	animsetName.uiRegion.y = 10;
	animsetName.font = Globals::primaryFont;
	uiElements.push_back(&animsetName);

	addButton.uiRegion.x = 10;
	addButton.uiRegion.y = 50;
	addButton.setup("Add Animation+");
	addButton.generateButton();
	uiElements.push_back(&addButton);

	jsonButton.uiRegion.x = 10;
	jsonButton.uiRegion.y = 100;
	jsonButton.setup("Export to JSON");
	jsonButton.generateButton();
	uiElements.push_back(&jsonButton);

	for (list<Animation>::iterator anim = animSet.animations.begin(); anim != animSet.animations.end(); anim++){
		
		addRow(anim->name);
	}

	anims.updateUIPositions();

	backButton.setup("Back", Globals::primaryFont);
	backButton.uiRegion.x = 10;
	backButton.generateButton();
	backButton.uiRegion.y = Globals::screenHeight - 10 - backButton.uiRegion.h;
	uiElements.push_back(&backButton);

	anims.setup(20, 150, Globals::screenWidth*0.6, backButton.uiRegion.y - 20 - 200);
	uiElements.push_back(&anims);

}

ManageAnimationSetScreen::~ManageAnimationSetScreen(){
	for (list<UIElement*>::iterator ui = anims.uiList.begin(); ui != anims.uiList.end(); ui++){
		UIRow* row = (UIRow*)(*ui);
		row->removeAll(true);
	}

	anims.removeAll(true);
}

void ManageAnimationSetScreen::uiUpdates(){
	if (addButton.clicked){
		addButton.clicked = false;

		CreateAnimationScreen createAnimScreen;
		createAnimScreen.update();

		if (createAnimScreen.animName != ""){
			//add to actual animationSet
			Animation animation(animSet.spriteSheet, createAnimScreen.animName);
			animSet.animations.push_back(animation);
			animSet.saveAnimationSet();

			//add to uilist
			addRow(createAnimScreen.animName);
		}
		exported = false;
	}
	if (jsonButton.clicked && !exported){
		exported = true;
		animSet.exportToJSON();
		jsonButton.clicked = false;
	}
	jsonButton.selected = exported;
	//for every item in the anims list, check if its been deleted or wants an edit
	for (list<UIElement*>::iterator ui = anims.uiList.begin(); ui != anims.uiList.end();){
		UIRow* row = (UIRow*)(*ui);
		//check the buttons in the row
		int rowCounter = 0; //0 is text, 1 is edit, 2 is delete
		bool deleteHappened = false;
		string animName;
		for (list<UIElement*>::iterator rowUI = row->uiElements.begin(); rowUI != row->uiElements.end(); rowUI++){
			if(rowCounter == 0){
				UIText* text = (UIText*)(*rowUI);
				animName = text->text;
			}
			else{
				BasicButton* btn = (BasicButton*)(*rowUI);
				if (btn->clicked){
					btn->clicked = false;
					if (rowCounter == 1)
					{
						//goto animation edit screen
						for (list<Animation>::iterator anim = animSet.animations.begin(); anim != animSet.animations.end(); anim++){
							if (anim->name == animName){
								ManageAnimationScreen manageAnimationScreen(&animSet, &(*anim));
								exported = false;
								manageAnimationScreen.update();
								break;
							}
						}
					}
					else{
						//delete this row from the ui list and the animlist
						for (list<Animation>::iterator anim = animSet.animations.begin(); anim != animSet.animations.end(); anim++){
							if (anim->name == animName){
								animSet.animations.erase(anim);
								animSet.saveAnimationSet();
								exported = false;
								break;
							}
						}

						deleteHappened = true;
						ui = anims.uiList.erase(ui);
					}

				}
				
			}
			rowCounter++;
		}
		if (!deleteHappened)
			ui++;
	}
	if (backButton.clicked){
		quit = true;
	}
}

void ManageAnimationSetScreen::addRow(string animName){
	UIRow* row = new UIRow;
	UIText* text = new UIText;
	text->text = animName;
	text->generateText();
	row->uiElements.push_back(text);

	BasicButton* editBtn = new BasicButton;
	editBtn->setup("Edit");
	editBtn->generateButton();
	row->uiElements.push_back(editBtn);

	BasicButton* deleteBtn = new BasicButton;
	deleteBtn->setup("X");
	deleteBtn->generateButton();
	row->uiElements.push_back(deleteBtn);

	row->updatePositions();

	anims.uiList.push_back(row);
}