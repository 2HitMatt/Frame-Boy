#include "deactivateButton.h"

void DeactivateButton::onClick(UIMouseEvent &mouseEvent){
	if (UIButton::checkMouseClick(*this, mouseEvent)){
		mouseEvent.mouseEventUsed = true;

		if (elementToDeactivate != NULL)
			elementToDeactivate->active = false;
	}
}