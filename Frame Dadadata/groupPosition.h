#ifndef GROUPPOS_H
#define GROUPPOS_H

#include "group.h"

class GroupPosition : public Group{
public:
	list<SDL_Point> data;

	GroupPosition(DataGroupType type){
		this->type = type;
	}
	int numberOfDataInGroup(){
		return data.size();
	}
	void outputGroup(ostream &stream, bool inGroups){
		for (list < SDL_Point>::iterator pt = data.begin(); pt != data.end(); pt++){

			if (!inGroups)
				stream << type.groupName << ": ";

			stream << (*pt).x << " " << (*pt).y << endl;
		}
	}
	void exportToJSON(ostream &stream, bool inGroups){
		stream << "\"" << type.groupName << "\":[";
		for (list < SDL_Point>::iterator pt = data.begin(); pt != data.end(); pt++){
			stream <<"{"<< Globals::jsonPointOutput(*pt)<<"}";

			list < SDL_Point>::iterator tmp = pt;
			tmp++;
			if (tmp != data.end()) 
				stream << "," << endl;
		
		}
		stream << "]";
	}
	void addToGroup(string str){
		//hope this works :/
		stringstream ss;
		ss << str;
		SDL_Point pt;
		ss >> pt.x >> pt.y ;

		data.push_back(pt);
	}

	void draw(){
		//TODO
	}

};


#endif