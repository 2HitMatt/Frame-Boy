#ifndef CREATERULESETSCREEN
#define CREATERULESETSCREEN

#include "uiScreen.h"
#include "uiTextInput.h"
#include "uiText.h"
#include "uiCheckbox.h"
#include "basicButton.h"
#include "uiList.h"
#include "addGroupScreen.h"
#include "uiRow.h"
#include "deactivateButton.h"
#include "uiHelpBox.h"

class CreateRulesetScreen : public UIScreen{
public:
	Ruleset newRuleset;

	UITextInput nameInput;
	//UITextInput extInput; ignoring file extension
	UICheckbox saveInGroupsCheckBox; //maaaaaaay omit this, it just means more shit to deal with in the end right?

	UIText groupsText;
	BasicButton addGroup;

	UIList groupsList;

	BasicButton buttonArray[15]; //for testing my list


	BasicButton saveButton;
	BasicButton cancelButton;

	UIHelpBox help;

	CreateRulesetScreen();
	void uiUpdates();

};

#endif