#ifndef STARTSCREEN
#define STARTSCREEN

#include "uiScreen.h"
#include "basicButton.h"
#include "exitButton.h"
#include "uiText.h"
#include "createRulesetScreen.h"
#include "manageRulesetScreen.h"
#include "createAnimationSetScreen.h"
#include "loadAnimationSetScreen.h"
#include "manageAnimationSetScreen.h"
#include "uiHelpBox.h"
#include "uiImage.h"
#include <Windows.h>

//first screen where we basically see some buttons and what the main ruleset is
class StartScreen : public UIScreen
{
public:

	UIText currentRuleSet;
	BasicButton createRuleSet;
	BasicButton manageRuleSets;
	BasicButton createAnimationSet;
	BasicButton loadAnimationSet;
	BasicButton helpButton;
	BasicButton websiteButton;
	BasicButton patreonButton;
	ExitButton exitButton;
	SDL_Texture* logoTexture;
	UIImage logo;
	UIHelpBox help;

	StartScreen();
	~StartScreen(){
		cleanup(logoTexture);
	}
	void uiUpdates();

};

#endif