#include "uiAnimation.h"

void UIAnimation::setup(Animation* anim, int x, int y){
	this->anim = anim;
	drawX = x;
	drawY = y;
	frame = anim->getFrame(0);

	updateUIRegion();
	

	//just in case
	TimeController::timerController.updateTime();
	TimeController::timerController.reset();

}
void UIAnimation::update(){
	if (frame != NULL){
		//draw animation loaded in
		counter += TimeController::timerController.dT;
		if (counter > frame->duration)
		{
			//currentFrame = anim->getNextFrame(currentFrame);
			frame = anim->getNextFrame(frame);
			counter = 0;
			updateUIRegion();

			
		}
	}
	else{
		frame = anim->getFrame(0);
		counter = 0;
	}
	TimeController::timerController.updateTime();
}
void UIAnimation::draw(){
	if (frame != NULL)
		frame->Draw(true, drawX - frame->offSet.x, drawY - frame->offSet.y);
}

void UIAnimation::updateUIRegion(){
	if (frame != NULL){
		uiRegion.x = drawX - frame->offSet.x;
		uiRegion.y = drawY - frame->offSet.y;
		uiRegion.w = frame->clip.w;
		uiRegion.h = frame->clip.h;
	}
}