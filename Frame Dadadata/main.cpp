#include <iostream>
#include <sstream>
#include <ctime> 
#include <cstdlib>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_mixer.h>
#include "logging.h"
#include "res_path.h"
#include "cleanup.h"
#include "drawing_functions.h"
#include "globals.h"
#include "ruleset.h"
#include "AnimationSet.h"
#include "startScreen.h"

//THE TODO LIST
/*
KEY
- top level
-- sub level
+ needed for first alpha
* done!

TODO
====
+-need to be able to rearrange frames in a list

*/


//to be 1080p, make screensize 1920 x 1080/960x540/480x270
const int SCREEN_WIDTH = 600; //30 tiles
const int SCREEN_HEIGHT = 400; //16.something :/ not even divisble by 32
const int TILE_SIZE = 40;

int main(int, char**){
	//load config file
	Globals::configFile.loadConfig();
	
	srand(time(0));

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0){//everything??? | SDL_INIT_GAMECONTROLLER
		logSDLError(std::cout, "SDL_Init");
		system("pause");
		return 1;
	}

	SDL_Window *window = SDL_CreateWindow("FRAME BOY", 100, 100, Globals::screenWidth,
		Globals::screenHeight, SDL_WINDOW_SHOWN);//| SDL_WINDOW_FULLSCREEN
	if (window == nullptr){
		logSDLError(std::cout, "CreateWindow");
		SDL_Quit();
		system("pause");
		return 1;
	}


	SDL_Renderer *render = SDL_CreateRenderer(window, -1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (render == nullptr){
		logSDLError(std::cout, "CreateRenderer");
		cleanup(window);
		SDL_Quit();
		system("pause");
		return 1;
	}

	Globals::renderer = render;

	//res independant
	//OMG, I just realised this is way cooler then I ever imagined. So I can set my screen size to whatever I want now, and leave this at the 
	//actual size of game. So logical size 300, 200, will 2 times scale on a window of 600, 400. FUUUCK, wish I knew this
	SDL_RenderSetLogicalSize(render, Globals::screenWidth, Globals::screenHeight);


	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG){
		logSDLError(std::cout, "IMG_Init");
		SDL_Quit();
		system("pause");
		return 1;
	}
	if (TTF_Init() != 0){
		logSDLError(std::cout, "TTF_Init");
		SDL_Quit();
		system("pause");
		return 1;
	}
	//Initialize SDL_mixer
	if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
	{
		logSDLError(std::cout, "Mixer_Init");
		SDL_Quit();
		system("pause");
		return 1;
	}

	//load bmp int sdl_surface(can colour palette this stage)
	const std::string resPath = getResourcePath();


	//SETUP FONTS
	string fontFile = resPath + "fonts/Audiowide-Regular.ttf";
	Globals::primaryFont = TTF_OpenFont(fontFile.c_str(), 18);
	Globals::secondaryFont = TTF_OpenFont(fontFile.c_str(), 14);
	fontFile = resPath + "fonts/smart_patrol_nbp.ttf";
	Globals::smallFont = TTF_OpenFont(fontFile.c_str(), 18);

	//Setup icon
	SDL_Surface* icon = loadSurface(resPath + "fboy2.png", render);
	SDL_SetWindowIcon(window, icon);
	cleanup(icon);

	//TESTING RULESET

	//build default ruleset (dont care about frame data basically)
	Ruleset test;
	test.name = "default";
	test.saveInGroups = false;
	test.outputNumberOfElementsInGroup = true;
	

	DataGroupType dgt1;
	dgt1.dataType = DataGroupType::DATATYPE_BOX;
	dgt1.groupName = "HitBox";
	dgt1.singleItem = true;

	DataGroupType dgt2;
	dgt2.dataType = DataGroupType::DATATYPE_STRING;
	dgt2.groupName = "Particle";
	dgt2.singleItem = false;

	test.groups.push_back(dgt1);
	test.groups.push_back(dgt2);


	test.saveRuleset();
	test.loadRuleset(test.name);
	cout << test.name << " " << test.fileExt << " " << test.saveInGroups << " " << test.outputNumberOfElementsInGroup << endl;
	for (list<DataGroupType>::iterator it = test.groups.begin(); it != test.groups.end(); it++)
	{
		cout << "Group: " << (*it).groupName << " " << (*it).dataType << " " << (*it).singleItem << endl;
	}

	//load existing ruleset now, orrrrr just use default
	Globals::currentRuleset = test;

	//TESTING FRAME
	Frame testFrame(NULL, 1, { 0, 0, 100, 100 }); //adding some test data
	GroupBox* hitboxes = (GroupBox*)GroupBuilder::findGroupByName("HitBox", testFrame.frameData);
	SDL_Rect box1 = { 0, 2, 30, 50 };
	hitboxes->data.push_back(box1);
	Group* particles = (GroupBox*)GroupBuilder::findGroupByName("Particle", testFrame.frameData);
	particles->addToGroup("explodo 34 34");
	
	ofstream file;
	file.open(resPath + "frames/testFrame.txt");
	
	testFrame.saveFrame(file);
	//Test: Look at txt exported
	file.close();

	//next - test load
	Frame loadTestFrame(NULL);
	ifstream infile;
	infile.open(resPath + "frames/testFrame.txt");
	loadTestFrame.loadFrame(infile);

	cout << "Frame in clip: " << loadTestFrame.clip.x << " " << loadTestFrame.clip.y << " " << loadTestFrame.clip.w << " " << loadTestFrame.clip.h << endl;

	infile.close();

	//TODO NEXT:
	//see if there is a way to go back to a position when reading a file

	
	AnimationSet animSet;
	animSet.imageName = "cyborg.bmp";
	animSet.outputFileName = "cyborg.txt";
	//maybe load it up too, meh
	
	Animation anim1(NULL, "Walk");
	anim1.frames.push_back(testFrame);
	Animation anim2(NULL, "Run");
	anim2.frames.push_back(loadTestFrame);

	animSet.animations.push_back(anim1);
	animSet.animations.push_back(anim2);

	animSet.saveAnimationSet();

	AnimationSet loadAnimSet;
	loadAnimSet.loadAnimationSet("cyborg.txt");

	Animation* walk = loadAnimSet.getAnimation("Walk");
	walk->name = walk->name;
	loadAnimSet.saveAnimationSet();


	//TESTING DONE, USE REAL DATAS NOW
	if (Globals::configFile.lastRuleSet != "")
		Globals::currentRuleset.loadRuleset(Globals::configFile.lastRuleSet);

	StartScreen startScreen;

	startScreen.update();


	SDL_Quit();

	//system("pause");
	return 0;
}