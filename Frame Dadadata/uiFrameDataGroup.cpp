#include "uiFrameDataGroup.h"

UIFrameDataGroup::UIFrameDataGroup(Group *group, UIImageEditor *editor){
	this->group = group;
	this->editor = editor;

	label.setup(group->type.groupName);
	label.uiRegion.x = uiRegion.x;
	label.uiRegion.y = uiRegion.y;
	label.generateButton();

	addButton.setup("Add+");
	addButton.uiRegion.x = uiRegion.x + label.uiRegion.w;
	addButton.uiRegion.y = uiRegion.y;
	addButton.generateButton();

	//depends on group type, but add all rows of data from list
	if (group->type.dataType == DataGroupType::DATATYPE_STRING){
		GroupString* groupStr = (GroupString*)group;

		for (list<string>::iterator str = groupStr->data.begin(); str != groupStr->data.end(); str++){
			addRow(*str);
		}
	}
	if (group->type.dataType == DataGroupType::DATATYPE_ROTATION){
		GroupRotation* groupRot = (GroupRotation*)group;

		for (list<float>::iterator rot = groupRot->data.begin(); rot != groupRot->data.end(); rot++){
			stringstream ss;
			ss << *rot;
			addRow(ss.str());
		}
	}
	else if (group->type.dataType == DataGroupType::DATATYPE_BOX){
		GroupBox* groupBox = (GroupBox*)group;
		//then do stuff with it
		for (list<SDL_Rect>::iterator box = groupBox->data.begin(); box != groupBox->data.end(); box++){
			stringstream ss;
			ss << box->x << " " << box->y << " " << box->w << " " << box->h;
			addRow(ss.str());
		}
	}
	else if (group->type.dataType == DataGroupType::DATATYPE_POSITION){
		GroupPosition* groupPos = (GroupPosition*)group;
		//then do stuff with it
		for (list<SDL_Point>::iterator pos = groupPos->data.begin(); pos != groupPos->data.end(); pos++){
			stringstream ss;
			ss << pos->x << " " << pos->y;
			addRow(ss.str());
		}
	}
	updatePositions();
}

void UIFrameDataGroup::deleteData(int row){
	int rowCounter = 0;
	//delete is annoying, because i gotta cycle through it all again
	if (group->type.dataType == DataGroupType::DATATYPE_STRING){
		GroupString* groupStr = (GroupString*)group;

		for (list<string>::iterator str = groupStr->data.begin(); str != groupStr->data.end(); str++){
			if (rowCounter == row)
			{
				groupStr->data.erase(str);
				break;
			}
			rowCounter++;
		}
	}
	if (group->type.dataType == DataGroupType::DATATYPE_ROTATION){
		GroupRotation* groupRot = (GroupRotation*)group;

		for (list<float>::iterator rot = groupRot->data.begin(); rot != groupRot->data.end(); rot++){
			if (rowCounter == row)
			{
				groupRot->data.erase(rot);
				break;
			}
			rowCounter++;
		}
	}
	else if (group->type.dataType == DataGroupType::DATATYPE_BOX){
		GroupBox* groupBox = (GroupBox*)group;
		//then do stuff with it
		for (list<SDL_Rect>::iterator box = groupBox->data.begin(); box != groupBox->data.end(); box++){
			if (rowCounter == row)
			{
				groupBox->data.erase(box);
				break;
			}
			rowCounter++;
		}
	}
	else if (group->type.dataType == DataGroupType::DATATYPE_POSITION){
		GroupPosition* groupPos = (GroupPosition*)group;
		//then do stuff with it
		for (list<SDL_Point>::iterator pos = groupPos->data.begin(); pos != groupPos->data.end(); pos++){
			if (rowCounter == row)
			{
				groupPos->data.erase(pos);
				break;
			}
			rowCounter++;
		}
	}
	updatePositions();
}

void UIFrameDataGroup::updateDataRow(int row){
	int rowCounter = 0;
	//delete is annoying, because i gotta cycle through it all again
	if (group->type.dataType == DataGroupType::DATATYPE_STRING){
		GroupString* groupStr = (GroupString*)group;

		for (list<string>::iterator str = groupStr->data.begin(); str != groupStr->data.end(); str++){
			if (rowCounter == row)
			{
				UITextInput* textInput = (UITextInput*)(UIElement::selectedUIElement);
				(*str) = textInput->inputText;
			}
			rowCounter++;
		}
	}
	else if (group->type.dataType == DataGroupType::DATATYPE_ROTATION){
		GroupRotation* groupRot = (GroupRotation*)group;

		for (list<float>::iterator rot = groupRot->data.begin(); rot != groupRot->data.end(); rot++){
			if (rowCounter == row)
			{
				stringstream ss;
				UITextInput* textInput = (UITextInput*)(UIElement::selectedUIElement);
				ss << textInput->inputText;
				ss >> (*rot);
			}
			rowCounter++;
		}
	}
	else if (group->type.dataType == DataGroupType::DATATYPE_BOX){
		GroupBox* groupBox = (GroupBox*)group;
		//then do stuff with it
		for (list<SDL_Rect>::iterator box = groupBox->data.begin(); box != groupBox->data.end(); box++){
			if (rowCounter == row)
			{
				if (lastSelected != UIElement::selectedUIElement){
					editor->box = (*box);
				}
				else{ 
					(*box) = editor->box; 
					
				}
				editor->drawBox = true;
				//TODO update the toggleButton
				stringstream ss;
				ss << box->x << " " << box->y << " " << box->w << " " << box->h;
				ToggleButton* btn = (ToggleButton*)UIElement::selectedUIElement;
				btn->text = ss.str();
				btn->generateButton();
			}
			rowCounter++;
		}
	}
	else if (group->type.dataType == DataGroupType::DATATYPE_POSITION){
		GroupPosition* groupPos = (GroupPosition*)group;
		//then do stuff with it
		for (list<SDL_Point>::iterator pos = groupPos->data.begin(); pos != groupPos->data.end(); pos++){
			if (rowCounter == row)
			{
				if (lastSelected != UIElement::selectedUIElement){
					editor->point = (*pos);
				}
				else{
					(*pos) = editor->point;
				}
				editor->drawPoint = true;
				stringstream ss;
				ss << pos->x << " " << pos->y ;
				ToggleButton* btn = (ToggleButton*)UIElement::selectedUIElement;
				btn->text = ss.str();
				btn->generateButton();
			}
			rowCounter++;
		}
	}
	updatePositions();
}

void UIFrameDataGroup::addRow(string text){
	UIRow* row = new UIRow;
	if (group->type.dataType == DataGroupType::DATATYPE_ROTATION || group->type.dataType == DataGroupType::DATATYPE_STRING){
		//add input text
		UITextInput* input = new UITextInput;
		input->inputMax = 16;
		input->inputText = text;
		if (group->type.dataType == DataGroupType::DATATYPE_ROTATION)
			input->numbersOnly = true;
		input->setup("   ", Globals::smallFont);
		input->updateText();
		input->generateButton();
		row->uiElements.push_back(input);
	}
	else{
		//must be a box or point, so slip in a toggle button
		ToggleButton *toggle = new ToggleButton();
		toggle->setup(text, Globals::smallFont);
		toggle->selectGlobally = true;
		toggle->generateButton();
		row->uiElements.push_back(toggle);
	}
	BasicButton* delBtn = new BasicButton;
	delBtn->setup("x", Globals::smallFont);
	delBtn->generateButton();
	row->uiElements.push_back(delBtn);

	row->updatePositions();

	data.push_back(row);
}
void UIFrameDataGroup::updatePositions(){
	label.uiRegion.x = uiRegion.x;
	label.uiRegion.y = uiRegion.y;

	addButton.uiRegion.x = uiRegion.x + label.uiRegion.w;
	addButton.uiRegion.y = uiRegion.y;
	
	int accumulativeHeight = label.uiRegion.h;
	for (list<UIRow*>::iterator row = data.begin(); row != data.end(); row++)
	{
		(*row)->uiRegion.x = uiRegion.x;
		(*row)->uiRegion.y = uiRegion.y + accumulativeHeight;
		(*row)->updatePositions();
		accumulativeHeight += (*row)->uiRegion.h;
	}
	uiRegion.h = accumulativeHeight;
}
void UIFrameDataGroup::onClick(UIMouseEvent &mouseEvent){
	if (checkMouseClick(*this, mouseEvent)){
		
		label.onClick(mouseEvent);
		addButton.onClick(mouseEvent);

		for (list<UIRow*>::iterator row = data.begin(); row != data.end(); row++){
			(*row)->onClick(mouseEvent);
		}
		
		
		//take it as used otherwise
		mouseEvent.mouseEventUsed = true;
	}
}
void UIFrameDataGroup::update(){
	label.update();
	addButton.update();
	int rowCounter = 0;
	
	for (list<UIRow*>::iterator row = data.begin(); row != data.end(); )
	{
		bool deleteHappened = false;

		(*row)->update();
		//agh, the hard part
		BasicButton* delBtn = (BasicButton*)(*row)->uiElements.back();
		if (delBtn->clicked){
			//ugh, delete the row somehow
			deleteHappened = true;

			deleteData(rowCounter);
			data.erase(row);
			break;

		}
		if (!deleteHappened){
			//check front button of the row
			
			if (group->type.dataType == DataGroupType::DATATYPE_BOX){
				ToggleButton* btn = (ToggleButton*)(*row)->uiElements.front();
				if (UIElement::selectedUIElement == btn && btn->selected){
					editor->editorMode = UIImageEditor::EDITORMODE_BOX;
					
					updateDataRow(rowCounter);
					
				}
			}
			else if (group->type.dataType == DataGroupType::DATATYPE_POSITION){
				ToggleButton* btn = (ToggleButton*)(*row)->uiElements.front();
				if (UIElement::selectedUIElement == btn && btn->selected){
					editor->editorMode = UIImageEditor::EDITORMODE_POINT;
					updateDataRow(rowCounter);
					
				}
			}
			else if (group->type.dataType == DataGroupType::DATATYPE_STRING){
				UITextInput* btn = (UITextInput*)(*row)->uiElements.front();
				if (UIElement::selectedUIElement == btn && btn->selected){
					editor->editorMode = UIImageEditor::EDITORMODE_MOVE;
					updateDataRow(rowCounter);
				}
			}
			else if (group->type.dataType == DataGroupType::DATATYPE_ROTATION){
				UITextInput* btn = (UITextInput*)(*row)->uiElements.front();
				if (UIElement::selectedUIElement == btn && btn->selected){
					editor->editorMode = UIImageEditor::EDITORMODE_MOVE;
					//maybe validate the numbers input?
					updateDataRow(rowCounter);
					
				}
			}

			rowCounter++;
			row++;
		}
	}

	//checking if buttons were pressed or we have to do something
	if (label.clicked){
		label.clicked = false;
		//if hiding data elements, unhide them
		//else
		//unhide them

	}
	if (addButton.clicked)
	{
		addButton.clicked = false;
		//if not single item OR is single item and we have 0 items in the group, then add a new row
		if (!group->type.singleItem || (group->type.singleItem && group->numberOfDataInGroup() <= 0)){
			addEmptyRow();
		}
	}
	
	updatePositions();
	lastSelected = UIElement::selectedUIElement;
}

void UIFrameDataGroup::addEmptyRow(){
	if (group->type.dataType == DataGroupType::DATATYPE_STRING){
		GroupString* groupStr = (GroupString*)group;

		groupStr->data.push_back("");
		addRow("");
		
	}
	if (group->type.dataType == DataGroupType::DATATYPE_ROTATION){
		GroupRotation* groupRot = (GroupRotation*)group;
		groupRot->data.push_back(0.0);

		addRow("0.0");
		
	}
	else if (group->type.dataType == DataGroupType::DATATYPE_BOX){
		GroupBox* groupBox = (GroupBox*)group;
		SDL_Rect empty = { 0, 0, 0 , 0 };
		groupBox->data.push_back(empty);
		addRow("0 0 0 0");
		
	}
	else if (group->type.dataType == DataGroupType::DATATYPE_POSITION){
		GroupPosition* groupPos = (GroupPosition*)group;
		SDL_Point point = { 0, 0 };
		groupPos->data.push_back(point);
		addRow("0 0");
		
	}
	updatePositions();
}

void UIFrameDataGroup::draw(){
	label.draw();
	addButton.draw();
	for (list<UIRow*>::iterator row = data.begin(); row != data.end(); row++)
	{
		(*row)->draw();
	}
}