#include "manageRulesetScreen.h"

ManageRulesetScreen::ManageRulesetScreen(){
	UIElement::selectedUIElement = NULL;


	label.text = "Choose Current RuleSet";
	label.font = Globals::primaryFont;
	label.uiRegion.x = 10;
	label.uiRegion.y = 10;
	label.generateText();
	uiElements.push_back(&label);
	
	string resPath = getResourcePath();
	Globals::filesInADir(rules, resPath + "rulesets");
	rules.size();

	for (list<string>::iterator ruleset = rules.begin(); ruleset != rules.end(); ruleset++)
	{
		BasicButton *rule = new BasicButton();
		rule->setup(*ruleset);
		rule->generateButton();
		rulesets.uiList.push_back(rule);
	}
	
	
	
	backButton.setup("Back", Globals::primaryFont);
	backButton.uiRegion.x = 10;
	backButton.generateButton();
	backButton.uiRegion.y = Globals::screenHeight - 10 - backButton.uiRegion.h;
	uiElements.push_back(&backButton);

	rulesets.setup(20, 100, Globals::screenWidth*0.6, backButton.uiRegion.y - 20 - 100);
	uiElements.push_back(&rulesets);
}
void ManageRulesetScreen::uiUpdates(){
	for (list<UIElement*>::iterator ruleBtn = rulesets.uiList.begin(); ruleBtn != rulesets.uiList.end(); ruleBtn++){
		BasicButton* btn = (BasicButton*)(*ruleBtn);
		if (btn->clicked)
		{
			Globals::currentRuleset.loadRuleset(Globals::getNameFromFile(btn->text));
			Globals::configFile.lastRuleSet = Globals::getNameFromFile(btn->text);
			Globals::configFile.saveConfig();
			quit = true;
			break;
		}
	}
	if (backButton.clicked)
	{
		quit = true;
	}
}