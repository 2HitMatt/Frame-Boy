#include "uiList.h"

void UIList::setup(int x, int y, int w, int h, bool scrollable){
	uiRegion.x = x;
	uiRegion.y = y;
	uiRegion.w = w;
	uiRegion.h = h;
	yOffsetMax = h; //default value for now, updates later with better values
	this->scrollable = scrollable;
	//to be able to render to a texture, we need to set its access to SDL_TEXTUREACCESS_TARGET ;)
	displayTexture = SDL_CreateTexture(Globals::renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, w, h);
}

void UIList::onClick(UIMouseEvent &mouseEvent){
	if (UIElement::touchesUIElement(*this, mouseEvent.x, mouseEvent.y)) {
		for (list<UIElement*>::iterator uiE = uiList.begin(); uiE != uiList.end(); uiE++){
			UIMouseEvent amendedMouseEvent = getAdjustedMouseEvent(mouseEvent);
			
			(*uiE)->onClick(amendedMouseEvent);

			
		}
		mouseEvent.mouseEventUsed = true;
	}
}
void UIList::onDrag(UIMouseEvent &mouseEvent){
	if (UIElement::touchesUIElement(*this, mouseEvent.x, mouseEvent.y)&& !mouseEvent.mouseEventUsed) {
		//this ones different, if there is an element we can drag in the list, we will(unlikely!)
		for (list<UIElement*>::iterator uiE = uiList.begin(); uiE != uiList.end(); uiE++){
			UIMouseEvent amendedMouseEvent = getAdjustedMouseEvent(mouseEvent);

			(*uiE)->onDrag(amendedMouseEvent);
		}
		//buuuuuuuut, if the mouseEvent wasn't used on any elements in the list, drag the whole list. Oh, if we can scroll that is
		if (!mouseEvent.mouseEventUsed && scrollable){
			yOffset += mouseEvent.yRel; //hopefully works?
			
			//restrict the scroll bounds
			if (yOffset < 0)
				yOffset = 0;
			if (yOffset > yOffsetMax)
				yOffset = yOffsetMax;

			
		}
		mouseEvent.mouseEventUsed = true;
	}
}
void UIList::onRelease(UIMouseEvent &mouseEvent){
	if (UIElement::touchesUIElement(*this, mouseEvent.x, mouseEvent.y) ) {
		for (list<UIElement*>::iterator uiE = uiList.begin(); uiE != uiList.end(); uiE++){
			UIMouseEvent amendedMouseEvent = getAdjustedMouseEvent(mouseEvent);

			(*uiE)->onRelease(amendedMouseEvent);

			
		}
		mouseEvent.mouseEventUsed = true;
	}
}

UIMouseEvent UIList::getAdjustedMouseEvent(UIMouseEvent &mouseEvent){
	//because our elements in this list are all relative to the scroller, we need to make an adjusted mouseEvent with x,y's that make sense to them
	UIMouseEvent amendedMouseEvent = mouseEvent;
	amendedMouseEvent.x = mouseEvent.x - uiRegion.x;
	amendedMouseEvent.y = mouseEvent.y - uiRegion.y;
	return amendedMouseEvent;
}

void UIList::updateUIPositions(){
	accumulativeHeight = 0;//as we find more elements, keep adding their heights onto this so we can work out the next elements y value
	int lastHeight = 0;
	for (list<UIElement*>::iterator uiE = uiList.begin(); uiE != uiList.end(); uiE++){
		(*uiE)->uiRegion.y = 0 - yOffset + accumulativeHeight;
		(*uiE)->uiRegion.x = 0;
		accumulativeHeight += (*uiE)->uiRegion.h;
		
	}
	//so scroll doesn't go too far
	if (accumulativeHeight > uiRegion.h){
		yOffsetMax = accumulativeHeight - uiRegion.h; //minus last height so we can always see atleast the last element on screen when scrolling lots
	}
	else
	{
		yOffsetMax = 0;
	}
}

void UIList::draw(){
	//tell all drawing/rendering functions to render to this display texture instead of directly to the screen
	SDL_SetRenderTarget(Globals::renderer, displayTexture);

	SDL_SetRenderDrawColor(Globals::renderer, 255/2, 255/2, 255/2, SDL_ALPHA_OPAQUE);
	SDL_RenderClear(Globals::renderer);

	//update their positions just in case
	updateUIPositions();
	//now draw all the suckers in our list
	for (list<UIElement*>::iterator uiE = uiList.begin(); uiE != uiList.end(); uiE++){
		(*uiE)->update();
		(*uiE)->draw();
	}

	//draw scroll bar
	if (scrollable){
		SDL_Rect scrollBar = { uiRegion.w - scrollBarWidth, 0, scrollBarWidth, uiRegion.h };
		SDL_SetRenderDrawColor(Globals::renderer, 47, 48, 36, SDL_ALPHA_OPAQUE);
		SDL_RenderFillRect(Globals::renderer, &scrollBar);

		//reuse scrollBar as the progress part now
		
		//percentage shown, then work out that in pixels over on our scroll bar
		scrollBar.h = (uiRegion.h / (uiRegion.h+yOffsetMax*0.1))*uiRegion.h;
		//then using this height, reset the y
		scrollBar.y = (yOffset / (yOffsetMax*1.0f))*uiRegion.h -uiRegion.h/2;

		SDL_SetRenderDrawColor(Globals::renderer, 255, 63, 0, SDL_ALPHA_OPAQUE);
		SDL_RenderFillRect(Globals::renderer, &scrollBar);
		
	}


	//reset it back to drawing to the screen
	SDL_SetRenderTarget(Globals::renderer, NULL);

	//now render the texture we've been building up
	renderTexture(displayTexture, Globals::renderer, uiRegion);
}

void UIList::removeInActive(bool deleteThem){
	for (list<UIElement*>::iterator uiE = uiList.begin(); uiE != uiList.end();){
		if (!(*uiE)->active){
			if (deleteThem)
				delete (*uiE);
			uiE = uiList.erase(uiE);
		}
		else
		{
			uiE++;
		}
	}
}

void UIList::removeAll(bool deleteThem){
	for (list<UIElement*>::iterator uiE = uiList.begin(); uiE != uiList.end();){
		if (deleteThem)
			delete (*uiE);
		uiE = uiList.erase(uiE);
	}
}