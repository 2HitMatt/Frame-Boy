#ifndef LOADANIMATIONSETSCREEN
#define LOADANIMATIONSETSCREEN

#include "uiScreen.h"
#include "uiList.h"
#include "uiRow.h"
#include "uiText.h"
#include "basicButton.h"


class LoadAnimationSetScreen : public UIScreen{
public:
	list<string> anims;
	
	UIText label;

	UIList animsets;
	BasicButton backButton;

	string animToLoad;

	LoadAnimationSetScreen();
	void uiUpdates();
};


#endif