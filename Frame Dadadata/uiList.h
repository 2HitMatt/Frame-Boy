#ifndef UILIST
#define UILIST

#include "uiElement.h"

//something that holds a list of ui elements :D
class UIList : public UIElement{
public:
	list<UIElement*> uiList;
	bool scrollable; //does dragging on the list slide it up and down
	int scrollBarWidth = 12; //if we can scroll, we will be drawing a scroll bar on the side
	int yOffset; //how much has the user scrolled all of this around
	int yOffsetMax; //how far are we allowed to scroll (needs to check in updatePositions). keep inmind scroll pulls elements up, so this will be a negative value
	SDL_Texture* displayTexture = NULL;//all of the ui elements are drawn to this texture for rendering (how we can clip elements ;) )
	int accumulativeHeight = 0;

	~UIList(){
		if (displayTexture != NULL)
			cleanup(displayTexture);
	}

	void setup(int x, int y, int w, int h, bool scrollable = true);
	
	void onClick(UIMouseEvent &mouseEvent);
	void onDrag(UIMouseEvent &mouseEvent);
	void onRelease(UIMouseEvent &mouseEvent);

	UIMouseEvent getAdjustedMouseEvent(UIMouseEvent &mouseEvent);

	void updateUIPositions();

	void draw();
	void removeInActive(bool deleteThem = false);
	void removeAll(bool deleteThem = false);
};

#endif