#include "loadAnimationSetScreen.h"

LoadAnimationSetScreen::LoadAnimationSetScreen(){
	UIElement::selectedUIElement = NULL;
	
	label.text = "Load Animation Set";
	label.font = Globals::primaryFont;
	label.uiRegion.x = 10;
	label.uiRegion.y = 10;
	label.generateText();
	uiElements.push_back(&label);

	string resPath = getResourcePath();
	Globals::filesInADir(anims, resPath + "frames", ".fdset");
	anims.size();

	for (list<string>::iterator animset = anims.begin(); animset != anims.end(); animset++)
	{
		BasicButton *anim = new BasicButton();
		anim->setup(*animset);
		anim->generateButton();
		animsets.uiList.push_back(anim);
	}
	
	

	/*BasicButton* testBtn = new BasicButton();
	testBtn->setup("Test1");
	testBtn->generateButton();
	animsets.uiList.push_back(testBtn);
	*/

	backButton.setup("Back", Globals::primaryFont);
	backButton.uiRegion.x = 10;
	backButton.generateButton();
	backButton.uiRegion.y = Globals::screenHeight - 10 - backButton.uiRegion.h;
	uiElements.push_back(&backButton);

	animsets.setup(20, 100, Globals::screenWidth*0.6, backButton.uiRegion.y - 20 - 200);
	uiElements.push_back(&animsets);

	animToLoad = "";
}
void LoadAnimationSetScreen::uiUpdates(){
	//check to see if the user has chosen an animation
	for (list<UIElement*>::iterator ui = animsets.uiList.begin(); ui != animsets.uiList.end(); ui++){
		BasicButton* btn = (BasicButton*)(*ui);
		if (btn->clicked){
			animToLoad = btn->text;
			quit = true;

			animsets.removeAll();

			break;
		}
	}
	if (backButton.clicked)
	{
		quit = true;
	}
}