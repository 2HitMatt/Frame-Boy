#include "createRulesetScreen.h"

CreateRulesetScreen::CreateRulesetScreen(){
	UIElement::selectedUIElement = NULL;


	nameInput.setup("Ruleset Name: ", Globals::primaryFont);
	nameInput.inputMax = 20;
	nameInput.uiRegion.x = 10;
	nameInput.uiRegion.y = 10;
	nameInput.generateButton();
	uiElements.push_back(&nameInput);

	saveInGroupsCheckBox.setup("Save in groups", Globals::primaryFont);
	saveInGroupsCheckBox.uiRegion.x = 10;
	saveInGroupsCheckBox.uiRegion.y = nameInput.uiRegion.y + nameInput.uiRegion.h + 10;
	saveInGroupsCheckBox.generateButton();
	uiElements.push_back(&saveInGroupsCheckBox);

	groupsText.text = "Groups: ";
	groupsText.font = Globals::primaryFont;
	groupsText.uiRegion.x = 10;
	groupsText.uiRegion.y = saveInGroupsCheckBox.uiRegion.y + saveInGroupsCheckBox.uiRegion.h + 10;
	uiElements.push_back(&groupsText);

	addGroup.setup("ADD +", Globals::secondaryFont);
	addGroup.uiRegion.x = 100;
	addGroup.uiRegion.y = groupsText.uiRegion.y;
	uiElements.push_back(&addGroup);

	saveButton.setup("Save", Globals::primaryFont);
	saveButton.uiRegion.x = 10;
	saveButton.generateButton();
	saveButton.uiRegion.y = Globals::screenHeight-10-saveButton.uiRegion.h;
	uiElements.push_back(&saveButton);

	cancelButton.setup("Cancel", Globals::primaryFont);
	cancelButton.uiRegion.x = 300;
	cancelButton.generateButton();
	cancelButton.uiRegion.y = Globals::screenHeight - 10 - saveButton.uiRegion.h;
	uiElements.push_back(&cancelButton);

	groupsList.setup(20, 150, Globals::screenWidth*0.6, saveButton.uiRegion.y - 20-150);
	uiElements.push_back(&groupsList);
	//for testing list
	for (int i = 0; i < 15; i++){
		string str = "Button ";
		buttonArray[i].setup(str);
		buttonArray[i].generateButton();
		//uncomment to fill list with buttons
		//groupsList.uiList.push_back(&buttonArray[i]);
	}


	help.setup(Globals::screenWidth / 2 + 4, 20, Globals::screenWidth / 2 - 8, 400);
	help.addRowOfText("HelpBox:");
	help.addRowOfText("Save in groups:");
	help.addRowOfText("example:- output if ticked");
	help.addRowOfText("     hitboxes: 2");
	help.addRowOfText("     10 20 40 89");
	help.addRowOfText("     45 3 67 23");
	help.addRowOfText("example:- output if not ticked");
	help.addRowOfText("     hitboxes: 10 20 40 89");
	help.addRowOfText("     hitboxes: 45 3 67 23");
	help.addRowOfText(" ");
	help.addRowOfText("Groups: Hold your data in your frames");
	help.addRowOfText("example:- hitboxes group holds boxes");
	//help.updateHeight();
	uiElements.push_back(&help);
}
void CreateRulesetScreen::uiUpdates(){
	//loop through my groups ui, to see if the user deleted any
	for (list<UIElement*>::iterator ui = groupsList.uiList.begin(); ui != groupsList.uiList.end(); ui++){
		//row is being setup to be deleted
		if (!(*ui)->active){
			UIRow*row = (UIRow*)(*ui);//cast this ui pointer to a row (coz we know its a row, otherwise if not, kablam!)

			//first element in our row is the text, we want this
			UIText* groupTextUi = (UIText*)row->uiElements.front();
			//now find this in our groups and destroy it
			for (list<DataGroupType>::iterator group = newRuleset.groups.begin(); group != newRuleset.groups.end(); group++){
				if ((*group).groupName == groupTextUi->text){
					//ok, we found the uiText name for the group in the actual group. Nice, now remove it
					newRuleset.groups.erase(group);
					break;
				}
			}
			//now that we are done with this thing, lets delete the entire row properly too
			row->removeAll(true);
			delete row;

			//and remove reference from ui list
			groupsList.uiList.erase(ui);
			break;
		}
	}
	
	if (saveButton.clicked)
	{
		if (nameInput.inputText != ""){
			//save, then quit
			
			newRuleset.name = nameInput.inputText;
			newRuleset.saveInGroups = saveInGroupsCheckBox.checked;
			newRuleset.outputNumberOfElementsInGroup = true;

			newRuleset.saveRuleset();
			UIElement::selectedUIElement = NULL;

			quit = true;
		}
		else
		{
			saveButton.clicked = false;
		}
	}
	else if (cancelButton.clicked)
	{
		UIElement::selectedUIElement = NULL;
		//dont save
		quit = true;
	}
	else if (addGroup.clicked)
	{
		UIElement::selectedUIElement = NULL;
		//go to add group screen, 
		AddGroupScreen addGroupScreen;
		addGroupScreen.update();

		//if they didn't cancel the last screen, do something with the group it made
		if (!addGroupScreen.cancelled && addGroupScreen.group.groupName != "")
		{
			newRuleset.groups.push_back(addGroupScreen.group);

			//build a row of this data now to manage this set
			UIRow *row = new UIRow();

			UIText* rowText = new UIText();
			rowText->text = addGroupScreen.group.groupName;
			rowText->font = Globals::smallFont;
			rowText->generateText();
			row->uiElements.push_back(rowText);

			DeactivateButton *deleteRow = new DeactivateButton();
			deleteRow->setup("Delete");
			deleteRow->generateButton();
			deleteRow->elementToDeactivate = row;
			row->uiElements.push_back(deleteRow);

			//I want the text part of this row to fill the void, so some maths
			rowText->uiRegion.w = groupsList.uiRegion.w - deleteRow->uiRegion.w - groupsList.scrollBarWidth;

			groupsList.uiList.push_back(row);
			
		}

		addGroup.clicked = false;
	}
	//FOR TESTING
	else if (buttonArray[0].clicked){
		//hey our text click works here, yea-ah!
		quit = true;
	}
}