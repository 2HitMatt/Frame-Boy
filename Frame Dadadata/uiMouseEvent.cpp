#include "uiMouseEvent.h"

const int UIMouseEvent::MOUSE_CLICK = 1;
const int UIMouseEvent::MOUSE_RELEASE = 2;
const int UIMouseEvent::MOUSE_DRAG = 3;

const int UIMouseEvent::MOUSE_LEFTBUTTON = 1;
const int UIMouseEvent::MOUSE_RIGHTBUTTON = 2;
const int UIMouseEvent::MOUSE_MIDDLEBUTTON = 3;

void UIMouseEvent::updateEvent(SDL_Event* event){
	//this is a new event, so lets just assume its not used yet
	mouseEventUsed = false;

	if (event->type == SDL_MOUSEBUTTONDOWN){
		x = event->button.x;
		y = event->button.y;
		
		if (event->button.button == SDL_BUTTON_LEFT)
			button = MOUSE_LEFTBUTTON;
		else if (event->button.button == SDL_BUTTON_RIGHT)
			button = MOUSE_RIGHTBUTTON;
		else
			button = MOUSE_MIDDLEBUTTON;
		

		type = MOUSE_CLICK;
	}
	if (event->type == SDL_MOUSEBUTTONUP){
		x = event->button.x;
		y = event->button.y;

		if (event->button.button == SDL_BUTTON_LEFT)
			button = MOUSE_LEFTBUTTON;
		else if (event->button.button == SDL_BUTTON_RIGHT)
			button = MOUSE_RIGHTBUTTON;
		else
			button = MOUSE_MIDDLEBUTTON;


		type = MOUSE_RELEASE;
	} 
	if (event->type == SDL_MOUSEMOTION){
		x = event->motion.x;
		y = event->motion.y;
		xRel = event->motion.xrel;
		yRel = event->motion.yrel;

		type = MOUSE_DRAG;
	}
}