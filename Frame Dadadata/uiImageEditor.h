#ifndef UIIMAGEEDITOR
#define UIIMAGEEDITOR

#include "uiImage.h"

class UIImageEditor : public UIElement{
public:
	static const int EDITORMODE_MOVE, EDITORMODE_POINT, EDITORMODE_BOX;

	int editorMode = EDITORMODE_MOVE;
	bool drawPoint = false;
	SDL_Point point;
	bool drawBox = false;
	SDL_Rect box;

	float xOffset = 0, yOffset = 0;
	const float zoomMax = 8;
	float zoom = 1;

	SDL_Texture* texture;
	SDL_Rect clip;

	void setup(SDL_Texture* texture, SDL_Rect clip);
	void update();
	void draw();

	void onClick(UIMouseEvent &mouseEvent);
	void onDrag(UIMouseEvent &mouseEvent);
	void onRelease(UIMouseEvent &mouseEvent);
	void onKeyPressed(SDL_Keycode key);
	

};

#endif