#ifndef BASICBUTTON
#define BASICBUTTON

#include "uiButton.h"

//all it does is register that someone clicked on it at all
class BasicButton : public UIButton{
public:
	bool clicked = false;

	void onClick(UIMouseEvent &mouseEvent);

};

#endif