#include "uiTextInput.h"

void UITextInput::generateButton(){
	string whitespace;
	for (int i = 0; i < inputMax; i++)
		whitespace += "M";
	
	buttonTextTexture = renderText(text, font, buttonTextColour, Globals::renderer);
	SDL_Texture* whiteSpaceTexture = renderText(whitespace, font, buttonTextColour, Globals::renderer);

	//work out new button size
	int W, H;
	SDL_QueryTexture(buttonTextTexture, NULL, NULL, &introTextWidth, &H);

	SDL_QueryTexture(whiteSpaceTexture, NULL, NULL, &W, NULL);

	uiRegion.w = introTextWidth + W + 4;
	uiRegion.h = H + 4;

	cleanup(whiteSpaceTexture);
}

void UITextInput::updateText(){
	if (inputTextTexture != NULL)
		cleanup(inputTextTexture);
	
	if (inputText != "")
		inputTextTexture = renderText(inputText, font, buttonTextColour, Globals::renderer);
	else
		inputTextTexture = renderText(" ", font, buttonTextColour, Globals::renderer);

}

void UITextInput::onClick(UIMouseEvent &mouseEvent){
	if (UIButton::checkMouseClick(*this, mouseEvent)){
		mouseEvent.mouseEventUsed = true;

		//make this selected
		selected = true;
		//if there is a selectedUIElement already, unselect it
		if (UIElement::selectedUIElement != NULL && UIElement::selectedUIElement != this)
			UIElement::selectedUIElement->selected = false;

		UIElement::selectedUIElement = this;
	}
}

void UITextInput::onTyped(char input){
	if (inputText.length() < inputMax){
		if (!numbersOnly || (numbersOnly && (isdigit(input) || input == '.'))){
			inputText += input;
			updateText();
		}
	}

}

void UITextInput::onBackSpace(){
	if (inputText.length() > 0)
	{
		inputText.pop_back();
		updateText();
	}
}

void UITextInput::copy(){
	SDL_SetClipboardText(inputText.c_str());
}
void UITextInput::paste(){
	inputText = SDL_GetClipboardText();
	updateText();
}

void UITextInput::useDefaultColours(){
	//bluey colour
	buttonColour.r = 17;
	buttonColour.g = 97;
	buttonColour.b = 111;

	//orange
	buttonBorderColour.r = 255;
	buttonBorderColour.g = 63;
	buttonBorderColour.b = 0;

	//lighter blue
	buttonSelectedColour.r = 12;
	buttonSelectedColour.g = 171;
	buttonSelectedColour.b = 180;

	//white
	buttonTextColour.r = 255;
	buttonTextColour.g = 255;
	buttonTextColour.b = 255;
}

void UITextInput::draw(){
	if (buttonTextTexture == NULL)
		generateButton();

	if (selected)
		SDL_SetRenderDrawColor(Globals::renderer, buttonSelectedColour.r, buttonSelectedColour.g, buttonSelectedColour.b, SDL_ALPHA_OPAQUE);
	else
		SDL_SetRenderDrawColor(Globals::renderer, buttonColour.r, buttonColour.g, buttonColour.b, SDL_ALPHA_OPAQUE);
	SDL_RenderFillRect(Globals::renderer, &uiRegion);

	SDL_SetRenderDrawColor(Globals::renderer, buttonBorderColour.r, buttonBorderColour.g, buttonBorderColour.b, SDL_ALPHA_OPAQUE);
	SDL_RenderDrawRect(Globals::renderer, &uiRegion);

	renderTexture(buttonTextTexture, Globals::renderer, uiRegion.x + 2, uiRegion.y + 2);
	if (inputTextTexture != NULL)
	{
		renderTexture(inputTextTexture, Globals::renderer, uiRegion.x + introTextWidth + 2, uiRegion.y + 2);
	}
}